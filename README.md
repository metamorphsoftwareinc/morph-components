# Curated Component Library #
Only inspected, approved components have the privilege of living here.

## What to store
Store only unzipped component packages. For example, if you have a *Resistor* with part number R1234, store it in `passive_components/resistors/single_components/R1234`, where the ACM file and other content begins within that R1234 directory.

## How to import stuff to the library

*High-Level Flow*

1. Create a branch for this set of changes
2. Do the technical steps below to integrate your components with the library
3. Once you're comfortable with the content of this branch, merge it to the master branch.

*Technical Flow*

0. In your terminal, navigate to the root folder of `morph-components`
1. If you don't already have a branch for your addition, create one `git checkout -b [new branch name]`
2. Run `git clean -xdf` to clean the repo
3. Run `git reset --hard HEAD` to remove any modifications to files in the repo
4. Run `python scripts/ImportComponentsToLibrary.py -l . --tree [path to tree of components to import]`
5. `git status` to see what has changed
    _NOTE: If you're importing a component that already exists in the library, then the old version will be deleted and the new one put in its place. If that happens, make sure it's what you intentional._
6. `git add --all .` to tell Git to "add all the new stuff, and mark all the missing stuff as deleted"
7. `git commit -m "Message"` to save the changes
8. `git push`
9. Think about the components in your branch. Test this new version of the library. Iterate until you're happy.
10. Switch to the master branch: `git checkout master`
11. Pull in those changes from your working branch `git merge --squash [name of that branch you were using] -m [message describing the set of components you're adding]`
12. `git push`

## Tests
A number of automated tests are provided for testing the integrity of component models.

### Test_Components script
To run: `python scripts/test_components/__init__.py`

This script runs a number of lightweight tests on component models, including:

- EDA Pins Connected
- Has Icon
- Has EDA Model
- Resource Files Exist
- No EDA Pin-to-Pin Connections
- Has Classification
- Has Datasheet
- Has Markdown
- Has MPN

### Packages test
To run: `python scripts/packages/packages.py -s .`

This script uses queries against Octopart and Digikey to check if a component model's package information matches the information found in these databases.