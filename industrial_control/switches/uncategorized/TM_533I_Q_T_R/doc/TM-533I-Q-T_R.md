# TM-533-T/R TACT Switch
## TM-533I-Q-T/R
### Industrial Control › Switches 
***

#### Description

Sharp click feel with a positive tactile feed-back. Due to small movement distance (stroke), user experiences distinct sensation when the switch clicks into place.

Ultraminiature and light weight structure suitable for high density mounting. Economic but high reliability.

Insert molding in the contact with special treatment prevents flux build-up during soldering and permits auto-dipping.

### Connectors 
- ***Name* []:** Functionality 


