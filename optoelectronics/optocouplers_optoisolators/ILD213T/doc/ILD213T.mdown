# Dual Optoisolator
## ILD213T
### Optoelectronics > Optocouplers/Optoisolators > Optoisolators
***

### Summary
OPTOISOLTR 4KV 2CH TRANS 8-SOIC

#### General Description
The ILD205T/206T/207T/211T/213T/217T are optically coupled pairs with a Gallium Arsenide infrared LED and a silicon NPN phototransistor. Signal information, including a DC level, can be transmitted by the device while maintaining a high degree of electrical isolation between input and output. The ILD205T/206T/207T/211T/213T/217T come in a standard SOIC-8 small outline package for surface mounting which makes it ideally suited for high density applications with limited space. In addition to eliminating through-holes requirements, this package conforms to standards for surface mounted devices.

A specified minimum and maximum CTR allows a narrow tolerance in the electrical design of the adjacent circuits. The high BVCEO of 70 V gives a higher safety margin compared to the industry standard of 30 V.

### Connectors 
- ***A1* [StdPin]:** Anode for LED 1
- ***A2* [StdPin]:** Anode for LED 2
- ***C1* [StdPin]:** Cathode for LED 1
- ***C2* [StdPin]:** Cathode for LED 2
- ***CT1* [StdPin]:** Collector for NPN phototransistor 1
- ***CT2* [StdPin]:** Collector for NPN phototransistor 2
- ***ET1* [StdPin]:** Emitter for NPN phototransistor 1
- ***ET2* [StdPin]:** Emitter for NPN phototransistor 2


