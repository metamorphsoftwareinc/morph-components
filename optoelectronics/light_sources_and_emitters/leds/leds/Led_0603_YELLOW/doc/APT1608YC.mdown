# 1.6X0.8mm SMD CHIP LED LAMP
## APT1608YC
### Optoelectronics > Light Sources and Emitters > LEDs > LEDs (Discrete)
***

### Summary
LED 1.6X0.8MM 588NM YEL CLR SMD

#### General Description
The Yellow source color devices are made with Gallium
Arsenide Phosphide on Gallium Phosphide Yellow Light
Emitting Diode.

### Connectors 
- ***Anode* [StdSignal]:** Anode of LED 
- ***Cath* [StdSignal]:** Cathode of LED

