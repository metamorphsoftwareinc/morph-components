# Bi-CMOS IC LED Boost Driver with PWM Dimming
## LV52205MUTBG
### Optoelectronics › Light Sources and Emitters › LEDs › Accessories › LED Drivers 
***

#### Description

The LV52205MU is a high voltage boost driver for LED drive. LED current is set by the external resistor R1 and LED dimming can be done by changing FB voltage with PWM control.

### Connectors 
- ***FB* [StdPin]:** Feedback pin. 
- ***FCAP* [StdPin]:** Filtering capacitor terminal for PWM signal. 
- ***SW* [DigitalSignal]:** Switch pin. Drain of the internal power FET.
- ***PWM* [DigitalSignal]:** PWM dimming input (active High). 
- ***VCC* [PwrGnd_TwoPort]:** Supply voltage.