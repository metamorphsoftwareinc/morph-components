#!/bin/bash -x

set -e

export PATH="$PATH":/c/python27
which python

export PYTHONUSERBASE=~/.morph-components-python
echo $PYTHONUSERBASE
python -m pip install --user beautifulsoup4 lxml requests numpy
python scripts/MetaPyUnit -x "$@"
