# Low-Cost I2C� Real-Time Clock/Calendar with SRAM
## MCP7940M-I/SN
### Semiconductors and Actives � Clock, Timing � Real Time Clocks 
***

#### Description
The MCP7940M series of low-power Real-Time Clocks (RTC) uses digital timing compensation for an accurate clock/calendar and a programmable output control for versatility. Using a low-cost 32.768 kHz crystal, it tracks time using several internal registers. for communication, the MCP7940M uses the I2C� bus.

The clock/calendar automatically adjusts for months with fewer than 31 days, including corrections for leap years. The clock operates in either the 24-hour or 12-hour format with an AM/PM indicator and settable alarm(s) to the second, minute, hour, day of the week, date or month. Using the programmable CLKOUT, frequencies of 32.768, 8.192 and 4.096 kHz and 1 Hz can be generated from the external crystal.

The RTC series of devices are available in the standard 8-lead SOIC, TSSOP, MSOP, PDIP, and 2x3 TDFN packages.

### Connectors 
- ***SDA* []:** Bidirectional Serial Data.
- ***SCL* []:** Serial Clock.
- ***X1* []:** Xtal Input, External Oscillator Input.
- ***X2* []:** Xtal Output.
- ***MFP* []:** Multi-Function Pin.
- ***VCC* []:** +1.8V to +5.5V Power Supply.
