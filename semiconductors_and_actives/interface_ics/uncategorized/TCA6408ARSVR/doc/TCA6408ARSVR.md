# LOW-VOLTAGE 8-BIT I2C AND SMBus I/O EXPANDER WITH INTERRUPT OUTPUT, RESET, AND CONFIGURATION REGISTERS
## TCA6408ARSVR
### Semiconductors and Actives › Interface ICs 
***

#### Description

This 8-bit I/O expander for the two-line bidirectional bus (I2C) is designed to provide general-purpose remote I/O expansion for most microcontroller families via the I2C interface [serial clock (SCL) and serial data (SDA)].

The major benefit of this device is its wide VCC range. It can operate from 1.65-V to 5.5-V on the P-port side and on the SDA/SCL side. This allows the TCA6408A to interface with next-generation microprocessors and
microcontrollers on the SDA/SCL side, where supply levels are dropping down to conserve power. In contrast to the dropping power supplies of microprocessors and microcontrollers, some PCB components such as LEDs
remain at a 5-V power supply.

The bidirectional voltage-level translation in the TCA6408A is provided through VCCI. VCCI should be connected to the VCC of the external SCL/SDA lines. This indicates the VCC level of the I2C bus to the TCA6408A. The voltage level on the P port of the TCA6408A is determined by VCCP.

The TCA6408A consists of one 8-bit Configuration (input or output selection), Input, Output, and Polarity Inversion (active high) Register. At power on, the I/Os are configured as inputs. However, the system master can enable the I/Os as either inputs or outputs by writing to the I/O configuration bits. The data for each input or output is kept in the corresponding Input or Output Register. The polarity of the Input Port Register can be inverted with the Polarity Inversion Register. All registers can be read by the system master.

The system master can reset the TCA6408A in the event of a timeout or other improper operation by asserting a low in the RESET input. The power-on reset puts the registers in their default state and initializes the I2C/SMBus state machine. The RESET pin causes the same reset/initialization to occur without depowering the part.

The TCA6408A open-drain interrupt (INT) output is activated when any input state differs from its corresponding Input Port Register state and is used to indicate to the system master that an input state has changed.

INT can be connected to the interrupt input of a microcontroller. By sending an interrupt signal on this line, the remote I/O can inform the microcontroller if there is incoming data on its ports without having to communicate via the I2C bus. Thus, the TCA6408A can remain a simple slave device.

The device P-port outputs have high-current sink capabilities for directly driving LEDs while consuming low device current.

One hardware pin (ADDR) can be used to program and vary the fixed I2C address and allow up to two devices to share the same I2C bus or SMBus.

### Connectors 
- ***VCCI* [SupplySingle]:** Supply voltage of I2C bus. Connect directly to the VCC of the external I2C master. Provides voltage level translation.
- ***ADDR* [DigitalSignal]:** Address input. Connect directly to VCCP or ground.
- ***RESET* [DigitalSignal]:** Active-low reset input. Connect to VCCI through a pullup resistor, if no active connection is used.
- ***P0* [DigitalSignal]:** P-port input/output (push-pull design structure). At power on, P0 is configured as an input.
- ***P1* [DigitalSignal]:** P-port input/output (push-pull design structure). At power on, P1 is configured as an input.
- ***P2* [DigitalSignal]:** P-port input/output (push-pull design structure). At power on, P2 is configured as an input.
- ***P3* [DigitalSignal]:** P-port input/output (push-pull design structure). At power on, P3 is configured as an input.
- ***P4* [DigitalSignal]:** P-port input/output (push-pull design structure). At power on, P4 is configured as an input.
- ***P5* [DigitalSignal]:** P-port input/output (push-pull design structure). At power on, P5 is configured as an input.
- ***P6* [DigitalSignal]:** P-port input/output (push-pull design structure). At power on, P6 is configured as an input.
- ***P7* [DigitalSignal]:** P-port input/output (push-pull design structure). At power on, P7 is configured as an input.
- ***INT* [DigitalSignal]:** Interrupt output. Connect to VCCI through a pullup resistor.
- ***SCL* [I2C]:** Serial clock bus. Connect to VCCI through a pullup resistor.
- ***SDA* [I2C]:** Serial data bus. Connect to VCCI through a pullup resistor.
- ***VCCP* [SupplySingle]:** Supply voltage of TCA6508A for P port.