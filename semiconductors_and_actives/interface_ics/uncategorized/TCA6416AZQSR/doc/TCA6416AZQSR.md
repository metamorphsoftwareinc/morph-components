# TCA6416A Low-Voltage 16-Bit I2C and SMBus I/O Expander With Voltage Translation, Interrupt Output, Reset Input, and Configuration Registers
## TCA6416AZQSR
### Semiconductors and Actives › Interface ICs 
***

#### Description

The TCA6416A is a 24-pin device that provides 16 bits of general purpose parallel input/output (I/O) expansion for the two-line bidirectional I2C bus (or SMBus) protocol. The device can operate with a power supply voltage ranging from 1.65 V to 5.5 V on the P-port side (VCCP).

The device supports both 100-KHz (Standard-mode) and 400-KHz (Fast-mode) clock frequencies. I/O expanders such as the TCA6416A provide a simple solution when additional I/Os are needed for switches, sensors, push-buttons, LEDs, fans, etc.

### Connectors 
- ***INT* [DigitalSignal]:** Interrupt output. Connect to VCCI or VCCP through a pull-up resistor. 
- ***VCCI* [PwrGnd_TwoPort]:** Supply voltage of I2C bus. Connect directly to the supply voltage of the external I2C master.
- ***RESET* [DigitalSignal]:** Active-low reset input. Connect to VCCI or VCCP through a pull-up resistor, if no active connection is used.
- ***P00* [PortBit0]:** P-port input/output (push-pull design structure). At power on, P00 is configured as an input.
- ***P01* [PortBit0]:** P-port input/output (push-pull design structure). At power on, P01 is configured as an input.
- ***P02* [PortBit0]:** P-port input/output (push-pull design structure). At power on, P02 is configured as an input.
- ***P03* [PortBit0]:** P-port input/output (push-pull design structure). At power on, P03 is configured as an input.
- ***P04* [PortBit0]:** P-port input/output (push-pull design structure). At power on, P04 is configured as an input.
- ***P05* [PortBit0]:** P-port input/output (push-pull design structure). At power on, P05 is configured as an input.
- ***P06* [PortBit0]:** P-port input/output (push-pull design structure). At power on, P06 is configured as an input.
- ***P07* [PortBit0]:** P-port input/output (push-pull design structure). At power on, P07 is configured as an input.
- ***P10* [PortBit0]:** P-port input/output (push-pull design structure). At power on, P10 is configured as an input.
- ***P11* [PortBit0]:** P-port input/output (push-pull design structure). At power on, P11 is configured as an input.
- ***P12* [PortBit0]:** P-port input/output (push-pull design structure). At power on, P12 is configured as an input.
- ***P13* [PortBit0]:** P-port input/output (push-pull design structure). At power on, P13 is configured as an input.
- ***P14* [PortBit0]:** P-port input/output (push-pull design structure). At power on, P14 is configured as an input.
- ***P15* [PortBit0]:** P-port input/output (push-pull design structure). At power on, P15 is configured as an input.
- ***P16* [PortBit0]:** P-port input/output (push-pull design structure). At power on, P16 is configured as an input.
- ***P17* [PortBit0]:** P-port input/output (push-pull design structure). At power on, P17 is configured as an input.
- ***ADDR* [DigitalSignal]:** Address input. Connect directly to VCCP or ground.
- ***SCL* [I2C]:** Serial clock bus. Connect to VCCI through a pull-up resistor.
- ***SDA* [I2C]:** Serial data bus. Connect to VCCI through a pull-up resistor.
- ***VCCP* [PwrGnd_TwoPort]:** Supply voltage of TCA6416A for P-ports.