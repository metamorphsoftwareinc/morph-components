# Single line low capacitance Transil™ for ESD protection
## ESDAVLC6-1BV2
### Semiconductors and Actives › Discretes › Diodes 
***

### Summary
TVS DIODE 3VWM 18VC FLIPCHIP

#### General Description
The ESDAVLC6-1BV2 is a single line bidirectional transil diode designed specially for the protection of integrated circuits into portable equipment and miniaturized electronics devices subject to ESD transient overvoltage.

The device is ideal for applications where both reduced printed circuit board space and high ESD protection level are required.

### Connectors 
- ***P1* [StdPin]:** ESD Terminal
- ***P2* [StdPin]:** ESD Terminal


