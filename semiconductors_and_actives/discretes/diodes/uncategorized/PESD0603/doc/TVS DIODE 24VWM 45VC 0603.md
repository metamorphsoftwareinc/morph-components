# ESD Protection Devices
## PESD0603-240
### Semiconductors and Actives › Discretes › Diodes 
***

### Summary
TVS DIODE 24VWM 45VC 0603

#### General Description
TE’s ESD line of devices help protect I/O ports on HDMI 1.3, portable video players, LCD & plasma TVs, USB 2.0, digital visual interface (DVI), and antenna switches. ESD devices shunt electrostatic discharge away from sensitive circuitry in HDTV equipment, printers, laptops, cellular phones, and other portable devices.

ESD devices offer many advantages over traditional protection devices, such as multi layer varistors (MLVs), which may degrade or distort the signal in high data rate circuits. Compared to transient voltage suppression (TVS) diodes and miniature gas discharge tubes (GDTs), ESD devices provide a more compact form factor and an economical solution for the shrinking profiles of today’s compact information appliances.

Available in a range of form factors, our ESD protection devices provide low capacitance, and meet transmission line pulse (TLP) testing, as well as IEC61000-4-2 testing.

### Connectors 
- ***1* [StdPin]:** ESD Terminal
- ***2* [StdPin]:** ESD Terminal


