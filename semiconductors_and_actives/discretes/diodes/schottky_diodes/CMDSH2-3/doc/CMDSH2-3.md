# SUPERmini SURFACE MOUNT SCHOTTKY DIODE HIGH CURRENT - 200mA
## CMDSH2-3 TR
### Semiconductors and Actives > Discretes > Schottky Diodes
***

### Summary
DIODE SCHOTTKY 30V 200MA SOD323

#### General Description
The CENTRAL SEMICONDUCTOR CMDSH2-3 type is a Silicon Schottky Diode, manufactured in a super-mini surface mount package, designed for fast switching applications requiring a low forward voltage drop.

### Connectors 
- ***A* [StdPin]:** Anode
- ***C* [StdPin]:** Cathode
