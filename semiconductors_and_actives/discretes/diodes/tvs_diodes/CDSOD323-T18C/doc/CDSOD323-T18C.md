# CDSOD323-TxxC - TVS Diode Array Series
## CDSOD323-T18C
### Semiconductors and Actives > Discretes > Diodes > TVS Diodes
***

### Summary
TVS DIODE 18VWM 29VC SMD

#### General Description
The markets of portable communications, computing and video equipment are challenging the semiconductor industry to develop increasingly smaller electronic components.

Bourns offers Transient Voltage Suppressor Array diodes for surge and ESD protection applications, in SOD323 package size format. The Transient Voltage Supressor Array series offers a choice of voltage types ranging from 3 V to 24 V in a unidirectional or bidirectional configuration. Bourns® Chip Diodes conform to JEDEC standards, are easy to handle on standard pick and place equipment and their flat configuration minimizes roll away.

The Bourns® device will meet IEC 61000-4-2 (ESD), IEC 61000-4-4 (EFT) and IEC 61000-4-5 (Surge) requirements.

Note: For 12 V and 24 V VDSL applications, the CDSOD323-TxxC-DSL family of devices is recommended.

### Connectors 
- ***C* [StdPin]:** Cathode
- ***A* [StdPin]:** Anode
