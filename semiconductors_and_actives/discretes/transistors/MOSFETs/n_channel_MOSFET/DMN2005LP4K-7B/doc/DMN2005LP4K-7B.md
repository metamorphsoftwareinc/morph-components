# N-Channel Enhancement Mode Mosfet
## DMN2005LP4K-7B
### Classification
***

#### Description

Low On-Resistance.

Very Low Gate Threshold Voltage, 0.9V Max.

Fast Switching Speed.

Low Input/Output Leakage.

Ultra-Small Surface Mount Package.

Totally Lead-Free & Fully RoHS Compliant.

Halogen and Antimony Free. "Green" Device.

ESD Protected Gate.

Ultra Low Profile Package.

Qualified to AEC-Q101 Standards for High Reliability.

### Connectors 
- ***DRAIN* [StdPin]:** Drain
- ***SOURCE* [StdPin]:** Source 
- ***GATE* [StdPin]:** Gate 

