# SN74AUP1G34 Low-Power Single Buffer Gate
## SN74AUP1G34YFPR
### Semiconductors and Actives › Logic, Standard › Logic Gates, Single 
***

### Summary
IC BUFFER GATE SGL 4-DSBGA

#### General Description
This single buffer gate performs the Boolean function Y = A in positive logic.

### Connectors 
- ***A* [AnalogSignal]:** Input A.
- ***Y* [AnalogSignal]:** Output Y.
- ***VCC* [PwrGnd_TwoPort]:** Power Pin.
