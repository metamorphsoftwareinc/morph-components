# 0.65-Ω Dual SPDT Analog Switches With Negative Signaling Capability
## TS5A22362YZPR
### Semiconductors and Actives › Logic, Standard › Switches 
***

#### Description

The TS5A22362 and TS5A22364 are single-pole double-throw (SPDT) analog switches designed to operate from 2.3 V to 5.5 V. The devices feature negative signal capability that allows signals below ground to pass through the switch without distortion. Additionally, the TS5A22364 includes an internal shunt switch, which automatically discharges any capacitance at the NC or NO terminals when they are unconnected to COM. This reduces the audible click/pop noise when switching between two sources. The break-before-make feature prevents signal distortion during the transferring of a signal from one path to another. Low ON-state resistance, excellent channel-to-channel ON-state resistance matching, and minimal total harmonic distortion (THD) performance are ideal for audio applications. The 3x3 mm DRC package is also available as a non-magnetic package for medical imaging application. 

### Connectors 
- ***V+* [PwrGnd_TwoPort]:** Functionality 
- ***NO1* [AnalogSignal]:** Functionality
- ***COM1* [AnalogSignal]:** Functionality
- ***IN1* [DigitalSignal]:** Functionality
- ***NO2* [AnalogSignal]:** Functionality
- ***COM2* [AnalogSignal]:** Functionality
- ***IN2* [DigitalSignal]:** Functionality