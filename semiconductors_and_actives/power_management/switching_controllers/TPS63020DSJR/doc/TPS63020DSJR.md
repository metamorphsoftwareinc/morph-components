# High Efficiency Single Inductor Buck-Boost Converter With 4-A Switches
## TPS63020DSJR
### Semiconductors and Actives › Power Management › Switching Controllers 
***

#### Description

The TPS6302x devices provide a power supply solution for products powered by either a two-cell or three-cell alkaline, NiCd or NiMH battery, or a one-cell Li-Ion or Li-polymer battery. Output currents can
go as high as 3A while using a single-cell Li-Ion or Li-Polymer Battery, and discharge it down to 2.5V or lower. The buck-boost converter is based on a fixed frequency, pulse-width-modulation (PWM) controller using synchronous rectification to obtain maximum efficiency. At low load currents, the converter enters Power Save mode to maintain high efficiency over a wide load current range. The Power Save mode can be disabled, forcing the converter to operate at a fixed switching frequency. The maximum average current in the switches is limited to a typical value of 4A. The output voltage is programmable using an external resistor divider, or is fixed internally on the chip. The converter can be disabled to minimize battery drain. During shutdown, the load is disconnected from the battery. The device is packaged in a 14-pin QFN PowerPAD™ package measuring 3 × 4 mm (DSJ).

### Connectors 
- ***EN* [DigitalSignal]:** Enable input (1 enabled, 0 disabled), must not be left open. 
- ***FB* [AnalogSignal]:** Voltage feedback of adjustable versions, must be connected to VOUT on fixed output voltage versions.
- ***L1* [AnalogSignal]:** Connection for inductor.
- ***L2* [AnalogSignal]:** Connection for inductor.
- ***PS/SYNC* [DigitalSignal]:** Enable/disable power save mode (1 disabled, 0 enabled, clocks signal for synchronization), must not be left open.
- ***PG* [DigitalSignal]:** Output power good (1 good, 0 failure; open drain).
- ***VIN* [SupplySingle]:** Supply voltage for power stage.
- ***VOUT* [AnalogSignal]:** Buck-boost converter output.
- ***VINA* [SuppySingle]:** Supply voltage for control stage.
- ***PowerPAD™* [Internally Connected]:** Must be connected to PGND. Must be soldered to achieve appropriate power dissipation.