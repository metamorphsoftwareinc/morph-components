# High Efficiency Step-Down Converter
## TPS82675SIPT
### Semiconductors and Actives › Power Management › DC - DC Converters
***

### Summary
600mA Fully Integrated, Low Noise Step-Down Converter Module in MicroSiP(TM) Package 8-uSiP

#### General Description
The TPS8267x device is a complete 600mA, DC/DC step-down power supply intended for low-power applications. Included in the package are the switching regulator, inductor and input/output capacitors. No additional components are required to finish the design. 

### Connectors 
- ***VOUT* [AnalogSignal]:** Output voltage, 1.2V
- ***EN* [DigitalSignal]:** Pull down to shutdown, pull up to enable
- ***MODE* [DigitalSignal]:** See datasheet for low and high operation
- ***VIN* [AnalogSignal]:** Current supply  for device
- ***GND* [GND]:** GND reference
