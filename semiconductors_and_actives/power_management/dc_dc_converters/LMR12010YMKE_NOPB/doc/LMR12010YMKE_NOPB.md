# LMR12010 SIMPLE SWITCHER® 20Vin, 1A Step-Down Voltage Regulator in SOT-23
## LMR12010YMKE/NOPB
### Semiconductors and Actives › Power Management › DC - DC Converters 
***

####Description

The LMR12010 regulator is a monolithic, high frequency, PWM step-down DC/DC converter in a 6-pin Thin SOT-23 package. It provides all the active functions to provide local DC/DC conversion with fast transient response and accurate regulation in the smallest possible PCB area.

With a minimum of external components and online design support through WEBENCH, the LMR12010 is easy to use. The ability to drive 1A loads with an internal 300mO NMOS switch using state-of-the-art -0.5μm BiCMOS technology results in the best power density available. The world class control circuitry allows for on-times as low as 13ns, thus supporting exceptionally high frequency conversion over the entive 3V to 20V input operating range down to the minimum output voltage of 0.8V. Switching frequency is internally set to 1.6 MHz (LMR12010X) or 3MHz (LMR1201Y), allowing the use of extremely small surface mount inductors and chip capacitors. Even though the operating frequencies are very high, efficiencies up to 90% are easy to achieve. External shutdown is included, featuring an ultra-low stand-by current of 30nA. The LMR12010 utilizes current-mode control and internal compensation to provide high-performance regulation over a wide range of operating conditions. Additional features include internal sotf-start circuitry to reduce inrush current, pulse-by-pulse current limit, thermal shutdown, and output over-voltage protection.

###Connectors 
- ***BOOST* []:** Boost voltage that drives the internal NMOS control switch. A bootstrap capacitor is connected between the BOOST and SW pins.
- ***FB* []:** Feedback pin. Connect FB to the external resistor divider to set output voltage.
- ***EN* []:** Enable control input. Logic high enables operation. Do not allow this pin to float or be greater than VIN + 0.3V.
- ***VIN* []:** Input supply voltage. Connect a bypass capacitor to this pin.
- ***SW* []:** Output switch. Connects to the inductor, catch diode, and bootstrap capacitor.
