# LMR10510 SIMPLE SWITCHER® 5.5Vin, 1A Step-Down Voltage Regulator in SOT-23 and WSON
## LMR10510YMFE/NOPB
### Semiconductors and Actives › Power Management › DC - DC Converters 
***

#### Description

The LMR10510 regulator is a monolithic, high frequency, PWM step-down DC/DC converter in a 5 pin SOT-23 and a 6 pin WSON package. It provides all the active functions to provide local DC/DC conversion with fast transient response and accurate regulation in the smallest possible PCB area. With a minimum of external components, the LMR10510 is easy to use. The ability to drive 1.0A loads with an internal 130 mO PMOS switch results in the best power density available. The world-class control circuitry allows on-times as low as 30ns, thus supporting exceptionally high frequency conversion over the entire 3V to 5.5V input operating range down to the minimum output voltage of 0.6V. The LMR10510 is a constant frequency PWM buck regulator IC that delivers a 1.0A load current. The regulator has a preset switching frequency of 1.6MHz or 3.0MHz. This high frequency allows the LMR10510 to operate with small surface mount capacitors and inductors, resulting in a DC/DC converter that requires a minimum amount of board space. The LMR10510 is internally compensated, so it is simple to use and requires few external components. Even though the operating frequency is high, efficiencies up to 93% are easy to achieve. External shutdown is included, featuring an ultra-low stand-by current of 30nA. The LMR10510 utilizes current-mode control and internal compensation to provide high-performance regulation over a wide range of operating conditions. Additional features include internal soft-start circuitry to reduce inrush current, pulse-by-pulse current limit, thermal shutdown, and output over-voltage protection.

### Connectors 
- ***FB* []:** Feedback pin. Connect to external resistor divider to set output voltage. 
- ***SW* []:** Switch node. Connect to the inductor and catch diode.
- ***VIND* []:** Power Input supply.
- ***VINA* []:** Control circuitry supply voltage. Connect VINA to VIND on PC board.
- ***EN* []:** Enable control input. Logic high enables operation. Do not allow this pin to float or be greater than VINA + 0.3V.
- ***DAP* []:** Functionality
