# 150-mA Low-noise Low-dropout Regulator with Shutdown
## LP2985-50DBVR
### Semiconductors and Actives › Power Management › Linear Regulators 
***

#### Description

The LP2985 family of fixed-output, low-dropout regulators offers exceptional, cost-effective performance for both portable and nonportable applications. Available in voltages of 1.8 V, 2.5 V, 2.8 V, 2.9 V, 3 V, 3.1 V, 3.3 V, 5 V, and 10 V, the family has an output tolerance of 1% for the A version (1.5% for the non-A version) and is capable of delivering 150-mA continuous load current. Standard regulator features, such as overcurrent and overtemperature protection, are included.

### Connectors 
- ***BYPASS* []:** Attach a 10-nF capacitor to improve low-noise performance.
- ***ON/OFF* []:** Active-low shutdown pin. Tie to VIN if unused.
- ***VIN* []:** Supply input.
- ***VOUT* []:** Voltage output.