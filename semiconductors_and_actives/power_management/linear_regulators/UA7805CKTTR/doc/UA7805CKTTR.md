# μA78xx Fixed Positive Voltage Regulators
## UA7805CKTTR
### Semiconductors and Actives › Power Management › Linear Regulators 
***

#### Description

This series of fixed-voltage integrated-circuit voltage regulators is designed for a wide range of applications. These applications include on-card regulation for elimination of noise and distribution problems associated with single-point regulation. Each of these regulators can deliver up to 1.5 A of output current. The internal current-limiting and thermal-shutdown features of these regulators essentially make them immune to overload. In addition to use as fixed-voltage regulators, these devices can be used with external components to obtain adjustable output voltages and currents, and also can be used as the power-pass element in precision regulators.

### Connectors 
- ***INPUT* []:** Supply Input.
- ***OUTPUT* []:** Voltage Output.

