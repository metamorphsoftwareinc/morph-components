# Miniature Single-Cell, Fully Integrated Li-Ion, Li-Polymer Charge Management Controllers
## MCP73832T-2ACI/OT
### Semiconductors and Actives › Power Management › Battery Management 
***

#### Description
The MCP73831/2 devices are highly advanced linear charge management controllers for use in space-limited, cost-sensitive applications. The MCP73831/2 are available in an 8-Lead, 2 mm x 3 mm DFN package or a 5-Lead, SOT-23 package. Along with their small physical size, the low number of external components required make the MCP73831/2 ideally suited for portable applications. For applications charging from a USB port, the MCP73831/2 adhere to all the specifications governing the USB power bus.

The MCP73831/2 employ a constant-current/constant-voltage charge algorithm with selectable preconditioning and charge termination. The constant voltage regulation is fixed with four available options: 4.20V, 4.35V, 4.40V or 4.50V, to accommodate new, emerging battery charging requirements. The constant current value is set with one external resistor. The MCP73831/2 devices limit the charge current based on die temperature during high power or high ambient conditions. This thermal regulation optimizes the charge cycle time while maintaining device reliability.

Several options are available for the preconditioning threshold, preconditioning current value, charge termination value and automatic recharge threshold. The preconditioning value and charge termination value are set as a ratio, or percentage, of the programmed constant current value. Preconditioning can be disabled. The MCP73831/2 are fully specified over the ambient temperature range of -40°C TO +85°C.

### Connectors 
- ***VDD* []:** Battery Management Input Supply.
- ***VBAT* []:** Battery Charge Control Output.
- ***STAT* []:** Charge Status Output.
- ***VSS* []:** Battery Management 0V Reference.
- ***PROG* []:** Current Regulation Set and Charge Control Enable.
- ***EP* []:** Exposed Thermal Pad (EP); must be connected to VSS.