# Low Noise 150 mA Low Drop Out (LDO) Linear Voltage Regulator
## MC78PC50NTRG
### Semiconductors and Actives › Power Management › Voltage Regulators 
***

#### Description

The MC78PC00 are a series of CMOS linear voltage regulators with high output voltage accuracy, low supply current, low dropout voltage, and high Ripple Rejection. Each of these voltage regulators consists of an internal voltage reference, an error amplifier, resistors, a current limiting circuit and a chip enable circuit.

The dynamic Response to line and load is fast, which makes these products ideally suited for use in hand-held communication equipment. The MC78PC00 series are housed in the SOT-23 5 lead package, for maximum board space saving.

### Connectors 
- ***VIN* []:** Input Pin.
- ***VOUT* []:** Output Pin. 
- ***CE* []:** Chip Enable Pin. 
