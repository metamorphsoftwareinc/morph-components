# Positive voltage regulator ICs
## L7805ACD2T-TR
### Semiconductors and Actives › Power Management › Voltage Regulators 
***

#### Description

The L78xx series of three-terminal positive regulators is available in TO-220, TO-220FP, TO-3, D2PAK and DPAK packages and several fixed output voltages, making it useful in a wide range of applications.

These regulators can provide local on-card regulation, eliminating the distribution problems associated with single point regulation. Each type employs internal current limiting, thermal shutdown and safe area protection, making it essentially indestructible. If adequate heat sinking is provided, they can deliver over 1 A output current. Although designed primarily as fixed voltage regulators, these devices can be used with external components to obtain adjustable voltage and currents.

### Connectors 
- ***OUTPUT* []:** Output. 
- ***INPUT* []:** Input.

