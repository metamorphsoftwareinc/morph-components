# 1.3 MHz/650kHz Step-Up DC/DC Converter in SC70, ThinSOT and DFN
## LT3460EDC-1#TRPBF
### Semiconductors and Actives › Power Management › Voltage Regulators 
***

### Summary
IC REG BOOST ADJ 0.18A 6DFN

#### General Description
The LT®3460/LT3460-1 are general purpose step-up DC/DC converters. The LT3460/LT3460-1 switch at 1.3MHz/650kHz, allowing the use of tiny, low cost and low height capacitors and inductors. The constant frequency results in low, predictable output noise that is easy to filter.

The high voltage switches in the LT3460/LT3460-1 are rated at 38V, making the device ideal for boost converters up to 36V. The LT3460 can generate 12V at up to 70mA from a 5V supply.

The low 1mA quiescent current and 650kHz switching frequency of LT3460-1 make it ideal for lower current applications.

The LT3460 is available in SC70 and SOT-23 packages.

The LT3460-1 is available in SC70 and 2mm × 2mm DFN
packages.

### Connectors 
- ***SW* [AnalogSignal]:** Switch Pin. Connect inductor/diode here. Minimize trace at this pin to reduce EMI.
- ***FB* [AnalogSignal]:** Feedback Pin. Reference voltage is 1.255V. Connect resistor divider tap here. Minimize trace area at FB. Set VOUT according to VOUT = 1.255V (1 + R1/R2).
- ***SHDN* [DigitalSignal]:** Shutdown Pin. Tie to 1.5V or higher to enable device; 0.4V or less to disable device. Also functions as soft-start. Use RC filter (47k, 47nF type).
- ***VIN* [AnalogSignal]:** Input Supply Pin. Must be locally bypassed. 
- ***GND* [GND]:** Ground Reference.
