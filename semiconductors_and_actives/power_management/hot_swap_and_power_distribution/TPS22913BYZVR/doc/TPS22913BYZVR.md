# TPS2291xx Ultra-small, Low On Resistance Load Switch With Controlled Turn-on
## TPS22913BYZVR
### Semiconductors and Actives › Power Management › Hot Swap and Power Distribution 
***

#### Description

The TPS22910A, TPS22912C, and TPS22913B/C are small, low rON load switches with controlled turn on. The device contains a P-channel MOSFET that can operate over an input voltage range of 1.4 V to 5.5 V. The switch is controlled by an on/off input (ON), which is capable of interfacing directly with low-voltage GPIO control signals. 

The TPS22910A, TPS22912C, and TPS22913B/C devices provide reverse current protection in ON and OFF states. An internal reverse voltage comparator disables the power-switch when the output voltage (VOUT) is driven higher than the input voltage (VIN), by VRCP, to quickly (10μs typ) stop the flow of current toward the input side of the switch. Reverse current protection is always active, even when the power-switch is disabled. Additionally, under-voltage lockout (UVLO) protection turns the switch off if the input voltage is too low.

The TPS22913B/C contains a 150-Ω on-chip load resistor for quick output discharge when the switch is turned off.

This family of devices have various slew rate options to avoid inrush current, are available in an ultra-small, space-saving 4-pin WCSP packages, and are characterized for operation over the free-air temperature range of –40°C to 85°C.

### Connectors 
- ***VOUT* [AnalogSignal]:** Switch Output.
- ***VIN* [SupplySingle]:** Switch input, use a bypass capacitor (ceramic) to ground.
- ***ON* [DigitalSignal]:** Switch control input. Do not leave floating.
