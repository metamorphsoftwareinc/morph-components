# Single-Channel, Adjustable Supervisory Circuit in Ultra-Small Package
## TPS3895ADRYR
### Semiconductors and Actives › Power Management › Voltage Supervisors 
***

#### Description

The TPS3895, TPS3896, TPS3897, and TPS3898 devices (TPS389x) are a family of very small supervisory circuits that monitor voltage greater than 500 mV with a 0.25% (typical) threshold accuracy and offer an adjustable delay time using external capacitors. The TPS389x family also has a logic enable pin (ENABLE) to power on/off the output. With the TPS3895, for example, when the input voltage pin (SENSE) rises above the threshold, and the enable pin (ENABLE) is high, then the output pin (SENSE_OUT) goes high after the capacitor-adjustable delay time. When SENSE falls below the threshold or ENABLE is low, then SENSE_OUT goes low.

For TPS389xA versions, both SENSE and ENABLE have a capacitor-adjustable delay. The output asserts after this capacitor adjustable delay when both SENSE and ENABLE inputs are good. The TPS389xP devices have a small, 0.2-μs propagation delay from when the enable asserts to when the output pin asserts, provided SENSE is above the threshold.

All devices operate from 1.7 V to 6.5 V and have a typical quiescent current of 6 μA with an open-drain output rated at 18 V. The TPS389x is available in an ultra-small μSON package and is fully specified over the temperature range of TJ = –40°C to +125°C.

### Connectors 
- ***ENABLE* [DigitalSignal]:** Active high input. 
- ***SENSE* [AnalogSignal]:** This pin is connected to the voltage that is monitored with the use of an external resistor.
- ***VCC* [SupplySingle]:** Supply voltage input.
- ***CT* [AnalogSignal]:** Capacitor-adjustable delay. 
- ***SENSE_OUT* [AnalogSignal]:** SENSE_OUT is an open-drain/push-pull output that is immediately driven low after V(SENSE) falls below VIT+ - Vhys or the ENABLE input is low. SENSE_OUT goes high after the capacitor-adjustable delay time when V(SENSE) is greater than VIT+ and the ENABLE pin is high. Open-drain devices (TPS3897/8) can be pulled up to 18 V independent of VCC; pull-up resistors are required for these devices.