# Dual 4A Ideal Diodes with Adjustable Current Limit
## LTC4415EDHC#PBF
### Semiconductors and Actives › Power Management 
***

#### Description

The LTC®4415 contains two monolithic PowerPath ideal diodes, each capable of supplying up to 4A with typical forward conduction resistance of 50mΩ. The diode voltage drops are regulated to 15mV during forward conduction at low currents, extending the power supply operating range and ensuring no oscillations during supply switchover. Less than 1μA of reverse current flows from OUT to IN, making this device well suited for power supply ORing applications.

The two ideal diodes are independently enabled and prioritized using inputs EN1 and EN2. The output current limits can be adjusted independently from 0.5A to 4A using resistors on the CLIM pins. Furthermore, the ideal diode currents can be monitored via CLIM pin voltages.

Open-drain status pins indicate when the ideal diodes are forward conducting. When the die temperature approaches thermal shutdown, or if the output load exceeds the current limit threshold, the corresponding warning pins are pulled low. 

### Connectors 
- ***IN1* [StdPin]:** Diode 1 Anode and Positive Power Supply for LTC4415. Bypass IN1 with a ceramic capacitor of at least 4.7μF. Pins 1 and 2 are fused together on the package. These pins can be grounded when not used.
- ***EN1* [DigitalSignal]:** Enable Input Diode 1. A high signal greater than VENTH enables Diode 1.
- ***CLIM1* [StdPin]:** Current Limit Adjust and Monitor Pin for Diode 1. Connect a resistor from CLIM1 to ground to set the current limit; the Diode 1 current can then be monitored by measuring the voltage on CLIM1 pin. A fixed 6A internal current limit is active when this pin is shorted to ground. Do not leave this pin open. Minimize stray capacitance on this pin to generally less than 200pF.
- ***CLIM2* [StdPin]:** Current Limit Adjust and Monitor Pin for Diode 2. Connect a resistor from CLIM2 to ground to set the current limit; the Diode 2 current can then be monitored by measuring the voltage on CLIM2 pin. A fixed 6A internal current limit is active when this pin is shorted to ground. Do not leave this pin open. Minimize stray capacitance on this pin to generally less than 200pF.
- ***EN2* [DigitalSignal]:** Enable Input for Diode 2. A low signal less than VENTH enables Diode 2.
- ***IN2* [StdPin]:** Diode 2 Anode and Positive Power SAupply for LTC4415. Bypass IN2 with a ceramic capacitor of at least 4.7μF. Pins 7 and 8 are fused together on the package. These pins can be grounded when not used.
- ***OUT2* [StdPin]:** Diode 2 Cathode and Output of LTC4415. Bypass OUT2 with a ceramic capacitor of at least 4.7μF. Pins 9 and 10 are fused together on the package. Leave these pins open when not used.
- ***STAT2* [DigitalSignal]:** Status indicator for Diode 2. Open-drain output pulls down during forward diode conduction. This pin can be left open or grounded when not used.
- ***WARN2* [DigitalSignal]:** Overcurrent and Thermal Warning Indicator for Diode 2. Open-drain output pulls down when Diode 2 current exceeds its current limit die temperature is close to thermal shutdown.
- ***WARN1* [DigitalSignal]:** Overcurrent and Thermal Warning Indicator for Diode 1. Open-drain output pulls down when Diode 1 current exceeds its current limit or die temperature is close to thermal shutdown.
- ***STAT1* [DigitalSignal]:** Status Indicator for Diode 1. Open-drain output pulls down during forward diode conduction. This pin can be left open or grounded when not used.
- ***OUT1* [StdPin]:** Diode 1 Cathode and Output of LTC4415. Bypass OUT1 with a ceramic capacitor of at least 4.7μF. Pins 15 and 16. are fused together on the package. Leave these pins open when not used.