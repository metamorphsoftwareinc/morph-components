# 2.6A Low Loss Ideal Diode in ThinSOT
## LTC4411
### Semiconductors and Actives
***

#### Description

The LTC®4411 is an ideal diode IC, capable of supplying up to 2.6A from an input voltage between 2.6V and 5.5V. The LTC4411 is housed in a 5-lead 1mm profile SOT-23 package.

The LTC4411 contains a 140mΩ P-channel MOSFET connecting IN to OUT. During normal forward operation, the drop across the MOSFET is regulated to as low as 28mV. Quiescent current is less than 40μA for load currents up to 100mA. If the output voltage exceeds the input voltage, the MOSFET is turned off and less than 1μA of reverse current flows from OUT to IN. Maximum forward current is limited to a constant 2.6A (typical) and internal thermal limiting circuits protect the part during fault conditions.

n open-drain STAT pin indicates conduction status. The STAT pin can be used to drive an auxiliary P-channel MOSFET power switch connecting an alternate power source when the LTC4411 is not conducting forward current.

An active-high control pin turns off the LTC4411 and reduces current consumption to less than 25μA. When shut off, the LTC4411 indicates this condition with a low voltage on the status signal.

### Connectors 
- ***IN* [StdPin]:** Ideal Diode Anode and Positive Power Supply for LTC4411. When operating LTC4422 as a switch it must be bypassed with a low ESR ceramic capacitor of 1μF. X5R and X7R dielectrics are preferred for their superior voltage and temperature characteristics.
- ***CTL* [StdPin]:** Controlled Shutdown Pin. Weak (3μA) Pull-Down. Pull this pin high to shut down the IC. Tie to GND to enable. Can be left floating when not in use.
- ***STAT* [StdPin]:** Status condition Indicator. This pin indicates the conducting status of the LTC4411. If the part is forward biased (VIN > VOUT + VFWD), this pin will be Hi-Z. If the part is reverse biased (VOUT > VIN + VRTO), then this pin will pull down 10μA through an open-drain. When terminated to a high voltage through a 470k resistor, a high voltage indicates diode conducting. May be left floating or grounded when not in use.
- ***OUT* [StdPin]:** Ideal Diode Cathode and Output of the LTC4411. Bypass OUT with a nominal 1mΩ ESR capacitor of at least 4.7μF. The LTC4411 is stable with ESRs down to 0.2mΩ. However, stability improves with higher ESRs.