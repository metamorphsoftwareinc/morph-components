N25Q016A11
===
###### Micron Serial NOR Flash Memory

###### [Datasheet](http://www.metamorphsoftware.com)

## Description
1.8V. SPI-compatible serial bus interface. 108 MHz clock frequency. 

## Connectors

- ***VCC* [PwrGnd_TwoPort]:** Power Supply
- ***VSS* [PwrGnd_TwoPort]:** Ground
- ***DQ0* [DigitalSignal]:** Serial Data Input and I/O
- ***DQ1* [DigitalSignal]:** Serial Data Output and I/O
- ***DQ2* [DigitalSignal]:** Input and I/O 
- ***DQ3* [DigitalSignal]:** Input and I/O
- ***CLOCK* [DigitalSignal]:** Clock
- ***S#* [DigitalSignal]:** Chip select (see datasheet for more info)

## Design
### Component Relationship

### Design Decisions

## Metadata
### Similar Components

### History of Changes