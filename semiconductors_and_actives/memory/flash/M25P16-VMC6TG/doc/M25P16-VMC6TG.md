# M25P16 Serial Flash Embedded Memory
## M25P16-VMC6TG
### Semiconductors and Actives › Memory 
***

### Summary
IC FLASH 16MBIT 75MHZ 8UFDFPN

#### General Description
The M25P16 is an 16Mb (2Mb x 8) serial Flash memory device with advanced write protection mechanisms accessed by a high speed SPI-compatible bus. The device supports high-performance commands for clock frequency up to 75MHz.

The memory can be programmed 1 to 256 bytes at a time using the PAGE PROGRAM command. It is organized as 32 sectors, each containing 256 pages. Each page is 256 bytes wide. Memory can be viewed either as 8,192 pages or as 2,097,152 bytes. The entire memory can be erased using the BULK ERASE command, or it can be erased one sector at a time using the SECTOR ERASE command.

### Connectors 
- ***VCC* [PwrGnd_TwoPort]:** Power Supply
- ***HOLD* [DigitalSignal]:** Used to pause serial communication
- ***WP* [DigitalSignal]:** Write Protect Mode
- ***SPI* [SPI]:** SPI Serial Bus

