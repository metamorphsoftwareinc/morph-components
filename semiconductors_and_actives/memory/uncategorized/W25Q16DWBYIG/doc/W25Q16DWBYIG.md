# 1.8V 16M-BIT SERIAL FLASH MEMORY WITH DUAL/QUAD SPI & QPI 
## W25Q16DWBYIG
### Semiconductors and Actives › Memory › Flash 
***

#### Description

The W25Q16DW (16M-bit) Serial Flash memory provides a storage solution for systems with limited space, pins and power. The 25Q series offers flexibility and performance well beyond ordinary Serial Flash devices. They are ideal for code shadowing to RAM, executing code directly from Dual/Quad SPI (XIP) and storing voice, text and data. The device operates on a single 1.65V to 1.95V power supply with current consumption as low as 4mA active and 1μA for power-down. All devices are offered in space-saving packages.

The W25Q16DW array is organized into 8,192 programmable pages of 256-bytes each. Up to 256 bytes can be programmed at a time. Pages can be erased in groups of 16 (4KB sector erase), groups of 128 (32KB block erase), groups of 256 (64KB block erase) or the entire chip (chip erase). The W25Q16DW has 512 erasable sectors and 32 erasable blocks respectively. The small 4KB sectors allow for greater flexibility in applications that require data and parameter storage. 

The W25Q16DW support the standard Serial Peripheral Interface (SPI), Dual/Quad I/O SPI as well as 2-clocks instruction cycle Quad Peripheral Interface (QPI): Serial Clock, Chip Select, Serial Data I/O0 (DI), I/O1 (DO), I/O2 (/WP), and I/O3 (/HOLD). SPI clock frequencies of up to 104MHz are supported allowing equivalent clock rates of 208MHz (104MHz x 2) for Dual I/O and 416MHz (104MHz x 4) for Quad I/O when using the Fast Read Dual/Quad I/O and QPI instructions. These transfer rates can outperform standard Asynchronous 8 and 16-bit Parallel Flash memories. The Continuous Read Mode allows for efficient memory access with as few as 8-clocks of instruction-overhead to read a 24-bit address, allowing true XIP (execute in place) operation.

A Hold pin, Write Protect pin and programmable write protection, with top or bottom array control, provide further control flexibility. Additionally, the device supports JEDEC standard manufacturer and device identification, a 64-bit Unique Serial Number and four 256-bytes Security Registers. 

### Connectors 
- ***/CS* [SPI]:** Chip Select Input. 
- ***DO (IO1)* [SPI]:** Data Output (Data Input Output 1) .
- ***/WP (IO2)* [DigitalSignal]:** Write Protect Input (Data Input Output 2).
- ***DI (IO0)* [SPI]:** Data Input (Data Input Output 0).
- ***CLK* [SPI]:** Serial Clock Input.
- ***/HOLD (IO3)* [DigitalSignal]:** Hold Input (Data Input Output 3). 
- ***VCC* [SupplySingle]:** Power Supply.