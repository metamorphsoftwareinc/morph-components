# Single-Supply, microPower CMOS Operational Amplifiers
## OPA336N/250
### Semiconductors and Actives › Amplifiers, Buffers › Operational Amplifiers (General Purpose) 
***

####Description
OPA336 series microPower CMOS operational amplifiers are designed for battery-powered applications. They operate on a single supply with operation as low as 2.1V. The output is rail-to-rail and swings to within 3mV of the supplies with a 100kO loiad. The common0mode range extends to the negative supply - ideal for single0supply applications. Single, dual, and quad versions have identical specifications for maximum design flexibility.

In addition to small size and low quiescent current (20µA/amplifier), they feature low offset voltage (125µV max), low input bias current (1pA), and high open-loop gain (115dB). Dual and quad designs feature completely independent circuitry for lowest crosstalk and freedom from interaction.

OPA336 packages are the tiny SOT23-5 surface-mount and SO-8 surface-mount. OPA2336 come in the miniature MSOP-8 surface-mount, SOP-8 surface-mount, and DIP-8 packages. The OPA4336 package is the space-saving SSOP-16 surface-mount.  All are specified from -40°C to +85°C. A macromodel is available for download (at www.ti.com) for design analysis.



###Connectors 
- ***Name* []:** Functionality 


