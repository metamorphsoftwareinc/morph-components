# FAB2210 — Audio Subsystem with Class-G Headphone and 3.3 W Mono Class-D Speaker with Dynamic Range Compression 
## FAB2210UCX
### Semiconductors and Actives > Amplifiers, Buffers > Audio Amplifiers > Headphone Amplifiers
***

### Summary
IC SUBSYSTEM HDPH AMP G 20-WLCSP

#### General Description
The FAB2210 combines a Class-G stereo capacitor-free headphone amplifier with a mono Class-D speaker amplifier into one IC package.

The headphone and speaker amplifiers incorporate Class-G and Class-D topologies, respectively, for low power dissipation, which extends battery runtime.

The Class-G headphone amplifier incorporates an integrated charge pump t
hat generates a negative supply rail for ground-centered headphone outputs.

The Class-D amplifier includes programmable Dynamic Range Compression (DRC) that maximizes Sound Pressure Level (SPL) for maximum loudness, while protecting the speaker from damage.

The noise gate can automatically mute the speaker or headphone amplifiers to reduce noise when input signals are LOW. 

### Connectors 
- ***SVDD* [PwrGnd_TwoPort]:** Power supply for Class-D amplifier.
- ***DVDD* [PwrGnd_TwoPort]:** Power supply for charge pump.
- ***I2C* [I2C]:** I2C serial interface.
- ***HVDD* [StdPin]:** Charge pump output; positive power supply for headphone amplifier, input preamplifiers and mixers.
- ***HVSS* [StdPin]:** Charge pump output; negative mirror of HVDD.
- ***CP+* [StdPin]:** Charge pump flying capacitor positive terminal.
- ***CP-* [StdPin]:** Charge pump flying capacitor negative terminal.
- ***INA1* [AnalogSignal]:** Single-ended line level audio input A1 (or non-inverting differential input INA+).
- ***INA2* [AnalogSignal]:** Single-ended line level audio input A2 (or inverting differential input INA-).
- ***INB1* [AnalogSignal]:** Single-ended line level audio input B1 (or non-inverting differential input INB+).
- ***INB2* [AnalogSignal]:** Single-ended line level audio input B2 (or inverting differential input INB-).
- ***HOUTL* [AnalogSignal]:** Left headphone amplifier output.
- ***HOUTR* [AnalogSignal]:** Right headphone amplifier output.
- ***HSENSE* [AnalogSignal]:** Sense ground; connect to DGND close to shield terminal of headphone jack.
- ***SOUT+* [AnalogSignal]:** Positive Class-D amplifier output.
- ***SOUT-* [AnalogSignal]:** Negative Class-D amplifier output.
