# UCSP, 1.8, Nanopower, Beyond-the-Rails Comparators With/Without Reference
## MAX9025EBT+T
### Semiconductors and Actives › Amplifiers, Buffers › Comparators 
***

#### Description

The MAX9025/MAX9028 nanopower comparators in space-saving chip-scale (UCSP™) packages feature Beyond-the-Rails™ inputs and are guaranteed to operate down to +1.8V. The MAX9025/MAX9026 feature an on-board 1.236V ±1% reference and draw an ultra-low supply current of only 1μA, while the MAX9027−MAX9028 (without reference) require just 0.6μA of supply current. These features make the MAX9025−MAX9028 family of comparators ideal for all 2-cell battery-monitoring/management applications.

The unique design of the output stage limits supply-current surges while switching, virtually eliminating the supply glitches typical of many other comparators. This design also minimizes overall power consumption under dynamic conditions. The MAX9025/MAX9027 have a push-pull output stage that sinks and sources current. Large internal-output drivers allow rail-to-rail output swing with loads up to 5mA. The MAX9026/MAX9028 have an open-drain output stage that makes them suitable for mixed-voltage system design. All devices are available in the miniature 6-bump UCSP packages.

Refer to the MAX9117 data sheet for similar comparators in 5-pin SC70 packages and the MAX9017 data sheet for similar dual comparators in 8-pin SOT23 packages.

### Connectors 
- ***OUT* [StdPin]:** Comparator Output 
- ***VCC* [PwrGnd_ThreePort]:** Positive Supply Voltage 
- ***VEE* [PwrGnd_ThreePort]:** Negative Supply Voltage 
- ***IN+* [StdPin]:** Comparator Noninverting Input 
- ***IN-* [StdPin]:** Comparator Inverting Input
- ***REF* [StdPin]:** 1.236V Reference Output