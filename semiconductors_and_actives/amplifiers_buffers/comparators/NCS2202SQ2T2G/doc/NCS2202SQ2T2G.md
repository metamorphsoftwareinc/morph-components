# Low Voltage Comparators
## NCS2202SQ2T2G
### Semiconductors and Actives › Amplifiers, Buffers › Comparators 
***

#### Description

The NCS2200 Series is an industry first sub−one volt, low power comparator family. These devices consume only 10 mA of supply current. They are guaranteed to operate at a low voltage of 0.85 V which allows them to be used in systems that require less than 1.0 V and are fully operational up to 6.0 V, which makes them convenient for use in both 3.0 V and 5.0 V systems. Additional features include no output phase inversion with overdriven inputs, internal hysteresis, which allows for clean output switching, and rail−to−rail input and output performance. The NCS2200 Series is available in complementary and open drain outputs and a variety of packages.

There are two industry standard pinouts for SOT−23−5 and SC70−5 packages. The NCS2200 is also available in the tiny DFN 2x2.2 package. The NCS2200A and NCS2202A are available in UDFN 1.2x1.0 package. 

### Connectors 
- ***VEE* [Supply_Dual]:** Negative Power Supply
- ***VCC* [Supply_Dual]:** Positive Power Supply
- ***IN-* [AnalogSignal]:** Inverting Input
- ***VOUT* [AnalogSignal]:** Output
- ***IN+* [AnalogSignal]:** Non-inverting Input