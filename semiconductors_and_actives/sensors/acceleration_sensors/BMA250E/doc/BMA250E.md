# Digital, triaxial acceleration sensor
## BMA250E
### Classification
***

#### Description

The BMA250E is a triaxial, low-g acceleration sensor with digital output for consumer applications. It allows measurements of acceleration in three perpendicular axes. An evaluation circuitry (ASIC) converts the output of a micromechanical acceleration-sensing structure (MEMS) that works according to the differential capacitance principle.

Package and interfaces of the BMA250E have been defined to match a multitude of hardware requirements. Since the sensor features an ultra-small footprint and a flat package, it is ingeniously suited for mobile applications.

The BMA250E offers a variable VDDIO voltage range from 1.2V to 3.6V and can be programmed to optimize functionality, performance, and power consumption in customer-specific applications.

In addition, it features an on-chip interrupt controller enabling motion-based applications without use of a microcontroller.

The BMA250E senses tilt, motion, inactivity, and shock vibration in cell phones, handhelds, computer peripherals, man-machine interfaces, virtual reality features and game controllers.

### Connectors 
- ***SDO* [SPI/I2C]:** Serial data output in SPI. Address select in I2C mode.
- ***SDx* [SPI/I2C]:** SDA serial data I/O in I2C. SDI serial data input in SPI 4W. SDA serial data I/O in SPI 3W.
- ***VDDIO* [PwrGnd_TwoPort_Digital]:** Digital I/O supply voltage (1.2V ... 3.6V).
- ***INT1* [DigitalSignal]:** Interrupt output 1.
- ***INT2* [DigitalSignal]:** Interrupt output 2.
- ***VDD* [PwrGnd_TwoPort_Analog]]:** Power supply for analog & digital domain (1.62V ... 3.6V).
- ***GNDIO* [PwrGnd_TwoPort_Digital]:** Ground for I/O.
- ***CSB* [SPI]:** Chip select for SPI mode.
- ***PS* [SPI/SPI_ThreeWire/PwrGnd_TwoPort_Digital]:** Protocol select (GND = SPI, VDDIO = I2C).
- ***SCx* [SPI,SPI_ThreeWire,I2C]:** SCK for SPI serial clock. SCL for I2C serial clock.