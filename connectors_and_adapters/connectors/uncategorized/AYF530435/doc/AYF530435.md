# FPC connectors (0.5mm pitch) Back lock
## AYF530435
### Connectors and Adapters › Connectors 
***

#### Description

Low profile, space saving back lock type with improved lever operability.

Mechanical design freedom achieved by top and bottom double contacts.

Wide selection, including a type with a small number of pins. Low profile and space saving design of 1.0 mm high and 3.20 mm deep (3.70 mm with lever). Y5B and Y5BW can have a minimum of four and two contacts respectively, maximum reduction in design packaging.

Wiring patterns can be located underneath the connector.

Man-hours for assembly can be reduced by delivering the connectors with their levers opened.

Y5BW features advanced functionality, including a structure to temporarily hold the FPC and a higher holding force. The FPC holding contacts located on both ends of the connector facilitate positioning of FPC and further enhance the FPC holding force.

### Connectors 
- ***1-4* [StdPin]:** FPC connector pins  


