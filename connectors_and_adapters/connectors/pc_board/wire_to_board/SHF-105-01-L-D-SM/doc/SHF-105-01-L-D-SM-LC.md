# SHROUDED & EJECTOR IDC HEADER
## SHF-105-01-L-D-SM-LC
### Connectors and Adapters > Connectors > PC Board/Wire to Board
***

### Summary
FFSD SHRD HDR W/O STRAIN RELIEF

#### General Description
Shrouded & Ejector IDC Header
Mates with:
FFSD, FFTP (SHF)

### Connectors 
- ***1* [StdPin]:** Pin 1
- ***2* [StdPin]:** Pin 2
- ***3* [StdPin]:** Pin 3
- ***4* [StdPin]:** Pin 4
- ***5* [StdPin]:** Pin 5
- ***6* [StdPin]:** Pin 6
- ***7* [StdPin]:** Pin 7
- ***8* [StdPin]:** Pin 8
- ***9* [StdPin]:** Pin 9
- ***10* [StdPin]:** Pin 10

[get from pin configuration]