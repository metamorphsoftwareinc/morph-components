# HCMOS SURFACE-MOUNT CRYSTAL CLOCK OSCILLATOR
## ASA-19.200MHZ-L-T
### Passive Components › Crystals/Resonators/Oscillators › Oscillators 
***

### Summary
OSC XO 19.20MHZ HCMOS TTL SMD

#### General Description

### Connectors 
- ***VDD* [PwrGnd_TwoPort]:** Power Supply
- ***19MHz* [DigitalClock]:** 19.2 MHz output
- ***TRI* [DigitalSignal]:** Tri-State switch


