# Cermet Trimmer
## 1623920-3
### Passive Components > Resistors > Potentiometers
***

### Summary
TRIMMER 100K OHM 0.15W TH

#### General Description
Low price, versatility and stable environmental characteristics are important benefits of this product. Featuring an innovative carbon resistive element printed on film in a fully enclosed package, this potentiometer style is normally chosen for a wide range of applications in industrial, professional and consumer equipment.

### Connectors 
- ***VOUT* [AnalogSignal]:** Output voltage 
- ***VCC* [PwrGnd_TwoPort]:** Supply voltage
- ***GND* [PwrGnd_TwoPort]:** Power supply ground


