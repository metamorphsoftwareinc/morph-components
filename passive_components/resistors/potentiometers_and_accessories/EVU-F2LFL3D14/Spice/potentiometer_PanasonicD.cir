*----- Positive "Logarithm" Potentiometer, Panasonic. Type: D -------
*
* Author: Tim Thomas
* Date: Sept 1 2015
*
* Panasonic shows three piecewise linear functions in their datasheet
* for the resistance dependency.
*
* 0.0 ------> 1.0
*        |2
*      __V__
*  1--|_____|--3
*        
*  o--R1-o-R2--o
*
.SUBCKT pot_panasonicD 1 2 3 PARAMS: Rtot=10000 wiper=0.5
* Parameters: Rtot, wiper
.param w=limit(0.01m,wiper,0.99999)
*
.param ratio=TABLE(w, 0,0.01m, 0.05,0.01m, 0.6,0.1, 0.65,0.125, 
+ 0.7,0.175, 1.0,0.99999)
*
R1 1 2 {Rtot*(ratio)}
R2 2 3 {Rtot*(1-ratio)}
.ENDS pot_panasonicD