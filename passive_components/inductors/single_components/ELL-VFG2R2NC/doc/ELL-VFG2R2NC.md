# Power Inductors / Wire Wound type
## ELL-VFG2R2NC
### Passive Components › Inductors › Single Components 
***

### Summary
FIXED IND 2.2UH 1.4A 87 MOHM SMD

#### General Description

### Connectors 
- ***P1* [StdPin]:** Terminal of inductor
- ***P2* [StdPin]:** Terminal of inductor


