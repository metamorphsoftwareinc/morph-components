ISL98607EIAZ-TR5616
===
###### High efficiency power supply

###### [Datasheet](http://www.metamorphsoftware.com)

## Description
This power supply offers two adjustable output rails at +-5V default. The I2C interface can be used to adjust up to +-5.7V with 50mV steps.

## Connectors

## Design
### Component Relationship

### Design Decisions

## Metadata
### Similar Components

### History of Changes