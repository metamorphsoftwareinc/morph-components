__author__ = 'adam'
from xml.dom import minidom
import sys
import os
import fnmatch
import shutil
import getopt


def get_classification_as_list(path):
    """
    @param path: The path to an ACM file
    @return: An ordered list with the component classifications, then name
    @rtype: list
    """

    print
    xmldoc = minidom.parse(path)
    name = xmldoc.childNodes[0]._attrs["Name"].firstChild.nodeValue

    itemlist = xmldoc.getElementsByTagName('Classifications')
    first_classification = itemlist[0]

    if first_classification is not None \
            and first_classification.firstChild is not None:
        str_class = first_classification.firstChild.nodeValue
        rtn = str_class.split('.')
        rtn.append(name)
    else:
        rtn = ["unclassified", name]

    return rtn


def convert_list_to_path(path_root, the_list):
    path = path_root

    for i in the_list:
        path = os.path.join(path, i)
    return path


def move_directory(src_path, dst_path):
    shutil.rmtree(dst_path, ignore_errors=True)
    shutil.copytree(src_path, dst_path)


def print_usage():
    print 'ImportComponentsToLibrary.py -t <input tree> -l <library root>'


def main(argv):
    src_path = None
    lib_root = None

    try:
        opts, args = getopt.getopt(argv, "ht:l:", ["tree=", "lib="])
    except getopt.GetoptError:
        print_usage()
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print_usage()
            sys.exit()
        elif opt in ("-t", "--tree"):
            src_path = arg
        elif opt in ("-l", "--lib"):
            lib_root = arg

    if src_path is None or lib_root is None:
        print_usage()
        sys.exit(2)

    print "Source Tree:    ", src_path
    print "Target Library: ", lib_root
    print ""

    # Check for directory existence
    if not os.path.exists(src_path):
        print "Source Tree path doesn't exist"
        exit(-1)
    if not os.path.exists(lib_root):
        print "Target Library path doesn't exist"
        exit(-1)

    matches = []
    for root, dirnames, filenames in os.walk(src_path):
        for filename in fnmatch.filter(filenames, '*.acm'):
            matches.append(os.path.join(root, filename))

    for acm_path in matches:
        classlist = get_classification_as_list(acm_path)
        newpath = convert_list_to_path(lib_root, classlist)
        print "moving: ", acm_path
        print "to: ", newpath
        print ""

        move_directory(os.path.dirname(acm_path), newpath)


if __name__ == "__main__":
    main(sys.argv[1:])