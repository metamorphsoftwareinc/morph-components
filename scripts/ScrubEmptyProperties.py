__author__ = 'adam'
from lxml import etree as ET
import sys
import os
import fnmatch
import shutil
import getopt
from multiprocessing import Pool

null_value = '-'

def strip_empty_properties(path):
    tree = ET.parse(path)
    root = tree.getroot()

    empty = []
    for prop in root.findall(".//Property"):
        v1 = prop.find('Value')
        ve = v1.find('ValueExpression')
        v2 = ve.find('Value')
        if v2.text == null_value:
            empty.append(prop)

    for e in empty:
        root.remove(e)

    tree.write(path)

def print_usage():
    print 'Scan a directory tree for ACM files, and delete Property objects with the value of "' + null_value + '"'
    print 'ScrubEmptyProperties.py -t <directory tree with ACMs>'


def main(argv):
    src_path = None

    try:
        opts, args = getopt.getopt(argv, "ht:", ["tree="])
    except getopt.GetoptError:
        print_usage()
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print_usage()
            sys.exit()
        elif opt in ("-t", "--tree"):
            src_path = arg

    if src_path is None:
        print_usage()
        sys.exit(2)

    print "ACM Directory Root: ", src_path
    print ""

    # Check for directory existence
    if not os.path.exists(src_path):
        print "ACM directory doesn't exist"
        sys.exit(-1)

    matches = []
    for root, dirnames, filenames in os.walk(src_path):
        for filename in fnmatch.filter(filenames, '*.acm'):
            matches.append(os.path.join(root, filename))

    print "About to process " + str(len(matches)) + " ACM files"

    p = Pool()
    result = p.map(strip_empty_properties, matches)

    print "Finished"


if __name__ == "__main__":
    main(sys.argv[1:])