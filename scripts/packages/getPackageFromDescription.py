__author__ = 'Henry'
###########################################################################
#
#    getPackageFromDescription
#
# Provides functions to find a package name from description text.
# Used when Octopart is otherwise missing package info for a part.
# See also: CT-68
#
###########################################################################
from sets import Set
import hashlib

packageSubstrings = [
    '01005',
    '0201',
    '0402',
    '0603',
    '0805',
    '1206',
    '1210',
    '1806',
    '1812',
    '2010',
    '2512',
    'D2PAK',
    'DDPAK',
    'DFN',
    'DFN1006',
    'DIP',
    'DSBGA',
    'HTSSOP',
    'LGA',
    'LLP',
    'METAL',
    'MICROFOOT'
    'MODULE',
    'MSOP',
    'PDIP',
    'QFN',
    'SC70JW',
    'SOD',
    'SOIC',
    'SON',
    'SOT',
    'SSOP',
    'TLGA',
    'TQFP',
    'TSOP',
    'TSOT',
    'TSSOP',
    'UDFN',
    'UFDFPN',
    'VLGA',
    'WLCSP',
    'WSON',
    'XFBGA',

    'SOT-23', 'SOT-346', 'SOT-323', 'SOT-416',
    'SOT-66', 'SOT-89', 'SOT-143', 'SOT-223', 'TSOT-23',
    'DO-204', 'DO-213', 'DO-214', 'SOD',

    # Transistor
    'SOT', 'TSOT', 'TO-3', 'TO-5', 'TO-18', 'TO-66', 'TO-92', 'TO-126', 'TO-220', 'TO-263', 'D2PAK',

    # Single row
    'SIP', 'SIL',

    # Dual row
    'DFN', 'DIP', 'DIL', 'SO', 'SOIC',  'SOP', 'SSOP', 'TSOP', 'TSSOP', 'ZIP',

    # Quad row
    'LCC', 'PLCC',  'QFN',  'QFP' 'QUIP', 'QUIL',

    # Grid array
    'BGA', 'LGA', 'PGA',

    # Wafer
    'COB', 'COF', 'COG', 'CSP', 'QP', 'UICC', 'WLP'
]

def getBestPackageFromDescriptions( descriptions ):
    packages = Set()

    for desc in descriptions:
        words = desc.replace(';', ' ').replace(',', ' ').split()
        candidates = [word for word in words if mightBePackage( word )]
        for candidate in candidates:
            packages.add( candidate )
    packageList = list( packages )
    packageList.sort(key=lambda x: packageScore( x ), reverse = True )
    bestSize = min(3,len(packageList))
    return packageList[0:bestSize]

# returns True if the word might be a package
def mightBePackage( word ):
    # Check for a match in our package substrings
    if word in packageSubstrings:
        return True
    # Only upper-case letters, digits, and hyphens are allowed in packages
    if len( word.strip('ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-') ) != 0:
        return False
    # check that there are at least two letters
    letterCount = sum(c.isalpha() for c in word)
    if letterCount < 2:
        return False
    # check that there is at least one digit
    digitCount = sum(c.isdigit() for c in word)
    if 0 == digitCount:
        return False
    return True

# computes a package score for a word.  The higher the score, the more the word seems like a package.
def  packageScore( word ):
    score = sum(c.isalpha() for c in word)  # Get the number of letters as a tie breaker
    score += len( list(set(word)) ) * 10   # Get the number of unique characters times ten as the basic score.
    bonus = [ len(x) for x in packageSubstrings if x in word]
    if len(bonus) > 0:
        score += max(bonus) * 100   # Add a bnus of 100 times the longest recognizable substring.
    return (score)

hashTags = [
    'ABBY', 'ABEL', 'ADAM', 'ADAN', 'AIDA', 'ALAN', 'ALBA', 'ALEX',
    'ALMA', 'ALTA', 'ALVA', 'AMIE', 'AMOS', 'ANDY', 'ANNA', 'ANNE',
    'AVIS', 'BART', 'BEAU', 'BERT', 'BESS', 'BETH', 'BILL', 'BOYD',
    'BRAD', 'BRET', 'CARA', 'CARI', 'CARL', 'CARY', 'CHAD', 'CLAY',
    'CLEO', 'CODY', 'COLE', 'CORA', 'CORY', 'CRUZ', 'CURT', 'DALE',
    'DANA', 'DANE', 'DARA', 'DAVE', 'DAWN', 'DEAN', 'DENA', 'DEON',
    'DICK', 'DINA', 'DION', 'DIRK', 'DONA', 'DORA', 'DOUG', 'DREW',
    'EARL', 'EDDY', 'EDNA', 'ELBA', 'ELDA', 'ELIA', 'ELLA', 'ELMA',
    'ELSA', 'ELVA', 'EMIL', 'EMMA', 'ENID', 'ERIC', 'ERIK', 'ERIN',
    'ERMA', 'ERNA', 'ETTA', 'EULA', 'EVAN', 'EZRA', 'FAYE', 'FERN',
    'FRAN', 'FRED', 'GAIL', 'GALE', 'GARY', 'GENA', 'GENE', 'GERI',
    'GINA', 'GLEN', 'GREG', 'GWEN', 'HANS', 'HONG', 'HOPE', 'HUGH',
    'HUGO', 'HUNG', 'INES', 'INEZ', 'IOLA', 'IONA', 'IRIS', 'IRMA',
    'IVAN', 'JACK', 'JADE', 'JAKE', 'JAME', 'JAMI', 'JANA', 'JANE',
    'JEAN', 'JEFF', 'JENA', 'JERI', 'JESS', 'JILL', 'JOAN', 'JODI',
    'JODY', 'JOEL', 'JOEY', 'JOHN', 'JONI', 'JOSE', 'JOSH', 'JUAN',
    'JUDI', 'JUDY', 'JUNE', 'KARA', 'KARI', 'KARL', 'KATE', 'KATY',
    'KAYE', 'KENT', 'KERI', 'KIRK', 'KRIS', 'KURT', 'KYLE', 'LACY',
    'LANA', 'LANE', 'LARA', 'LEAH', 'LELA', 'LENA', 'LEON', 'LESA',
    'LETA', 'LEVI', 'LILA', 'LILY', 'LINA', 'LISA', 'LIZA', 'LOIS',
    'LOLA', 'LONA', 'LORA', 'LORI', 'LOYD', 'LUCY', 'LUIS', 'LUKE',
    'LULA', 'LUPE', 'LYLE', 'LYNN', 'MACK', 'MARA', 'MARC', 'MARI',
    'MARK', 'MARY', 'MATT', 'MIKE', 'MINA', 'MINH', 'MONA', 'MYRA',
    'NEAL', 'NEIL', 'NELL', 'NEVA', 'NICK', 'NINA', 'NITA', 'NOAH',
    'NOEL', 'NOLA', 'NONA', 'NORA', 'OLGA', 'OMAR', 'OPAL', 'OTIS',
    'OTTO', 'OWEN', 'PAUL', 'PETE', 'PHIL', 'RAUL', 'REBA', 'REED',
    'REID', 'RENA', 'RENE', 'REVA', 'RHEA', 'RICH', 'RICK', 'RITA',
    'RORY', 'ROSA', 'ROSE', 'ROSS', 'RUBY', 'RUDY', 'RUSS', 'RUTH',
    'RYAN', 'SANG', 'SARA', 'SAUL', 'SCOT', 'SEAN', 'SETH', 'STAN',
    'SUNG', 'TAMI', 'TARA', 'TERA', 'TERI', 'THAD', 'THEO', 'TINA',
    'TOBY', 'TODD', 'TONI', 'TONY', 'TORI', 'TROY', 'VEDA', 'VERA',
    'VERN', 'VIDA', 'VITO', 'WADE', 'WARD', 'WILL', 'YONG', 'ZANE',
]

def getHashTag( data ):
    number = int( hashlib.sha256(data).hexdigest()[0:7], 16 )
    suffix = (number & 0xff) % 97 + 2
    index = (number >> 8) % len( hashTags )
    name = hashTags[ index ].title()
    return (name + str( suffix ))
