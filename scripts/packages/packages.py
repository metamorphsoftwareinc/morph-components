#!/usr/bin/python
import pprint

__author__ = 'Henry'

# Program to check the names of Eagle packages in GME components against Octopart specs for the MPN.
# python packages --src componentDirectory --dst resultsFilePath --map packageMapFilePath
#
# componentDirectory, and its sub-folders, are searched for ACM files.
# resultsFilePath names a CSV file that will get the test results.
# packageMapFilePath names a text file containing lines of comma-or-space-delimited synonymous package names, one group per line.
# It has the Octopart part numbers as column headers in the first row, and Eagle matches in the column below the appropriate header.
#
# Example script parameters to run from "morph-components\scripts\packages":
#
# To check Operational Amplifiers, the debug script parameters could be:
#  --src "..\..\semiconductors_and_actives\amplifiers_buffers\operational_amplifiers" --dst ".\package_results.csv" --map ".\package_map.txt"
# with the working directory (debug environment) set to:
# C:\Users\Henry\repos\morph-components\scripts\packages
# and the script set to:
# C:\Users\Henry\repos\morph-components\scripts\packages\packages.py
#

############## import files
import sys
import os
import errno
from os.path import exists, join
from optparse import OptionParser
import csv
import re
import shutil
import Queue
import threading
import time
import xml.etree.ElementTree as ET
import json
import urllib
import urllib2
import requests
from BeautifulSoup import BeautifulSoup
#import urllib2
import numpy as np
from getPackageFromDescription import getBestPackageFromDescriptions, getHashTag
import unicodedata
import copy


# Create a queue for Octopart requests
octopartRequestQueue = Queue.Queue(0)

# Create a queue for Digi-Key requests
dkRequestQueue = Queue.Queue(0)
dkRequestQueueLock = threading.Lock()

# Create a queue for final package-name analysis
fpaRequestQueue = Queue.Queue(0)

# Keep track of threads and thread IDs
threads = []
threadID = 1
threadExitFlag = 0
q2dkEvent = threading.Event()

def unicodeToString( x ):
    return unicodedata.normalize('NFKC',x).encode('latin-1', 'ignore' )

# Create multithreading class for Octopart lookups
class octopartLookupThread (threading.Thread):
    def __init__(self, threadID, name, q):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.name = name
        self.q = q
    def run(self):
        print "Starting a new thread to lookup Octopart packages (%s)." % (self.name)
        processOctopartRequest(self.name, self.q)
        print "Exiting the thread to lookup Octopart packages (%s)." % (self.name)

# Looks up Octopart part numbers to get package names
# Octopart's API is limited to 3 HTTP requests/second per IP address.
# Each Octopart HTTP request is limited to 20 mpns.
def processOctopartRequest(threadName, q):
    global q2dkEvent
    MAX_MPNS_PER_OCTOPART_HTTP_QUERY = 20

    while not threadExitFlag:
        countOfMpnsThisQuery = 0;
        chunkToDo = []
        while( not octopartRequestQueue.empty()) and (countOfMpnsThisQuery < MAX_MPNS_PER_OCTOPART_HTTP_QUERY):
            countOfMpnsThisQuery += 1;
            data = q.get()
            chunkToDo.append( data )
            q.task_done()
        if countOfMpnsThisQuery > 0:
            print "Getting Octopart info for %s MPNs." % (countOfMpnsThisQuery)

            chunkResults = getMultipleOctopartPackages( copy.deepcopy(chunkToDo) ) # Do the lookups

            for chunk in chunkResults:
                # queue data for Digi-Key package lookup.
                dkRequestQueueLock.acquire()
                dkRequestQueue.put( copy.deepcopy(chunk) )
                dkRequestQueueLock.release()
                q2dkEvent.set()
            countOfMpnsThisQuery = 0;
            time.sleep( 0.350 )  # Throttle Octopart's query rate.
        else:
            time.sleep( 0.050 )  # Don't hog CPU while waiting for something in our queue.
            # # print "%s processing %s" % (threadName, data)
            # package, dkpn = getOctopartPackageFromMpn( data['octopartMpn'])
            # data['octopartPackage'] =  package
            # data['digiKeyPartNumber'] = dkpn
            # print "The Octopart package for the %s is %s." % (data['octopartMpn'], data['octopartPackage'])
            # # queue data for Digi-Key package lookup.
            # dkRequestQueueLock.acquire()
            # dkRequestQueue.put( data )
            # dkRequestQueueLock.release()

        time.sleep(1)

# Starts the Octopart request-queue handling thread
def startOctopartLookupThread( threadId ):
    tName = "octopartLookupThread_" + str( threadId )
    thread = octopartLookupThread(threadId, tName, octopartRequestQueue)
    thread.start()
    threads.append(thread)

##########################################################################################################
# Create multithreading class for Digi-Key lookups
class dkLookupThread (threading.Thread):
    def __init__(self, threadID, name, q):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.name = name
        self.q = q
    def run(self):
        print "Starting a new thread to lookup Digi-Key packages (%s)." % (self.name)
        processDkRequest(self.name, self.q)
        print "Exiting the thread to lookup Digi-Key packages (%s)." % (self.name)

# Looks up Digi-Key part numbers to get package names
def processDkRequest(threadName, q):
    while not threadExitFlag:
        dkRequestQueueLock.acquire()
        if not dkRequestQueue.empty():
            data = q.get()
            dkRequestQueueLock.release()
            # print "%s processing %s" % (threadName, data)
            packageList = []
            for dkpn in data['digiKeyPartNumber']:
                package = getDkPackageFromMpnAndDkpn( data['octopartMpn'], dkpn )
                packageList = list(set(packageList + [package]))
                if len(package) > 1:
                    break;  # Stop on the first DKPN found, to reduce DK queries.
            if( len(packageList) == 0):
                print "Missing Digi-Key package list, data = " + str(data)
            data['digiKeyPackage'] =  packageList
            fpaRequestQueue.put( data )
            print "The Digi-Key package for the %s is %s." % (data['octopartMpn'], data['digiKeyPackage'])
            q.task_done()
        else:
            dkRequestQueueLock.release()
        time.sleep(0.050)

# Starts the Digi-Key request-queue handling thread
def startDkLookupThread( threadId ):
    tName = "digiKeyLookupThread_" + str( threadId )
    thread = dkLookupThread(threadId, tName, dkRequestQueue)
    thread.start()
    threads.append(thread)


##########################################################################################################
# Get package width in mm
def getPackageWidthInMillimeters( dkCase ):
    width = 0
    test = dkCase.upper()
    if('WIDTH' in test) and ('SOIC' in test):

        widthList = test.split()

        for w in widthList:
            if 'MM' in w:
                w = w.replace('M','')

                try:
                    width = float( w )
                except ValueError:
                    pass
    return width

# Get a SOIC width suffix from a width in mm
def getSoicWidthSuffixFromWidthInMillimeters( width ):
    suffix = ''
    if width < 3.4:
        if width <= 0:
            pass    # treat 0 width -- probably missing width -- as if narrow.
        else:
            suffix = 'mini'
    else:
        if width > 4.7:
            suffix = 'W'
        else:
            # suffix = 'N'
            pass    # Treat narrow packages as the default case
    return suffix

# Add a width indication to SOIC package names, based on a width in Digi-Key's "Package / Case" field.
def appendWidthToSoicPackageString( dkPackage, dkCase):
    appendedPackage = dkPackage
    packageWidth = getPackageWidthInMillimeters( dkCase )
    widthSuffix = getSoicWidthSuffixFromWidthInMillimeters( packageWidth )
    if( len(widthSuffix) > 0):
        appendedPackage = appendedPackage + '-' + widthSuffix
    return appendedPackage


##########################################################################################################
# Get the Digi-Key package name
def getDkPackageFromMpnAndDkpn( mpn, dkpn ):
    dkPackage = ''
    dkCase = ''
    dkPhotos = ''

    safeMpn = urllib.quote(mpn, '')
    safeDkpn = urllib.quote(dkpn, '')

    # Create Url to read
    # Digikey_url = 'http://digikey.com/scripts/DkSearch/dksus.dll?Detail&name='
    dkUrlBase = 'http://www.digikey.com/product-detail/en/'

    url = dkUrlBase + safeMpn + '/' + safeDkpn

    # Create BeautifulSoup Object
    # print "URL = " + url

    ok = False
    retryCount = 0
    maxRetries = 30
    retryDelay = 10

    # Loop to automatically retry accessing Digi-Key if there is a problem.
    while not ok:
        try:
            retryCount += 1
            resp = requests.get(url, timeout=20)
            ok = True
        except:
            print "Error getting url = %s." % (url)
            if retryCount > maxRetries:
                print "Too many retries, goodbye."
                sys.exit(-1)
            else:
                print "Retry #%s in %s seconds." % (retryCount, retryDelay)
                time.sleep( retryDelay )

    page = resp.text

    # Close Page
    resp.close()

    bs = BeautifulSoup(page)

    table = bs.find(lambda tag: tag.name=='table' and tag.has_key('class') and tag['class']=="product-additional-info")

    if 'LM4040AIM3-2.0' in mpn:
        print 'Special test case found'

    try:
        # Get the supplier package
        th = table.find('th', text=u'Supplier Device Package')
        td = th.findNext('td')
        dkPackage = unicodeToString( td.text )
    except:
        pass

    try:
        # Get the package / case
        th = table.find('th', text=u'Package / Case')
        td = th.findNext('td')
        dkCase = unicodeToString( td.text )
    except:
        pass

    try:
        # Get the product photos
        th = table.find('th', text=u'Product Photos')
        td = th.findNext('td')
        dkPhotos = unicodeToString( td.text )
    except:
        pass

    # Handle Digi-Key pages with a missing or null 'Supplier Device Package' field.
    if len(dkPackage) < 3:
        if len(dkCase) < 3:
            dkCase = dkPhotos
        dkPackageList = dkCase.strip().replace(',',' ').split()
        if len(dkPackageList) > 0:
            dkPackage = dkPackageList[ 0 ]
    else:
        dkPackageList = dkPackage.strip().replace(',',' ').split()
        if len(dkPackageList) > 1:
            dkPackage = dkPackageList[ 0 ]

    # Check for SOIC packages, to add a width indicator.  See also: CT-68.
    if ('SOIC' in dkPackage) and ('SOIC' in dkCase):
        dkPackage = appendWidthToSoicPackageString( dkPackage, dkCase)

    # Check for an exposed pad, to add an 'EP' indicator
    upCase = dkCase.upper()
    upPack = dkPackage.upper()
    if ('EXPOSED' in upCase) and ('PAD' in upCase) and (not ('-EP' in upPack)) and ( not ('-POWERPAD' in upPack)):
        upPack += '-EP'
    sz = len(upPack)
    if sz < 4:
        print "This shouldn't happen"                                           ''
    return( upPack )

##########################################################################################################
# Do a batch lookup of Octopart MPNs
def getMultipleOctopartPackages( chunkToDo ):
    chunkArray = np.asarray(chunkToDo)
    queries = []
    API_KEY = "22becbab"

    arraySz = len(chunkArray)
    rollCall = [False for i in range(arraySz)]

    # Make a list of queries.  See: https://octopart.com/api/docs/v3/overview#bom-matching
    for index, data in enumerate(chunkArray):
        obj = {}
        obj['mpn'] = data['octopartMpn']
        obj['reference'] = str( index )
        queries.append( obj )

    # Send queries to Octopart
    url = 'http://octopart.com/api/v3/parts/match?queries=%s' % urllib.quote(json.dumps(queries))
    url += '&apikey=' + API_KEY
    url += '&include[]=specs'
    url += '&include[]=descriptions'

    data = urllib2.urlopen(url, timeout=10).read()
    response = json.loads(data)

    for result in response['results']:
        referenceIndex = int( result['reference'] )
        rollCall[ referenceIndex ] = True
        mpn = chunkArray[ referenceIndex ]['octopartMpn']

        octopartPackage = []
        dkpn = []
        mouserPn = []
        descriptions = []
        octopartResultMpn = []

        foundExactMpn = False

        for item in result['items']:
            itemMpn = unicodeToString( item['mpn'] )
            if normalizePackageString( itemMpn ) == normalizePackageString( mpn ):
                if itemMpn != mpn:
                    print "Instead of '%s', found '%s' in Octopart results." % (mpn, itemMpn)
                    octopartResultMpn.append( itemMpn )
                else:
                    foundExactMpn = True

                # get the Octopart package name
                try:
                    newPackage = item['specs']['case_package']['value']
                    newPincount = item['specs']['pin_count']['value']
                    newPackageWithPins = combinePackageAndPins( newPackage, newPincount )
                    octopartPackage = list(set(octopartPackage + newPackageWithPins))
                except KeyError:
                    # Better luck next time.
                    # print "Skipped item missing package: %s" % (item)
                    pass

                # get the Octopart descriptions
                try:
                    newDescriptions = item['descriptions']
                    for newDesc in newDescriptions:
                        descriptions.append( unicodedata.normalize('NFKC',newDesc['value']).encode('latin-1', 'ignore' ))
                except KeyError:
                    # Better luck next time.
                    # print "Skipped item missing package: %s" % (item)
                    pass

                # Get the Digi-Key part number
                try:
                    for offer in item['offers']:
                        if offer['seller']['name'] == 'Digi-Key':
                            newDkpn = offer['sku']
                            dkpn = list(set(dkpn + [newDkpn.encode('latin-1')]))
                except KeyError:
                    # Better luck next time.
                    # print "Skipped item missing offers: %s" % (item)
                    pass

                # Get the Mouser part number
                try:
                    for offer in item['offers']:
                        if offer['seller']['name'] == 'Mouser':
                            newPn = offer['sku']
                            mouserPn = list(set(mouserPn + [newPn.encode('latin-1')]))
                except KeyError:
                    # Better luck next time.
                    # print "Skipped item missing offers: %s" % (item)
                    pass

                if len(dkpn) == 0:
                    # Octopart is missing Digi-Key part numbers for this part.
                    # Check if Digi-Key really  does have the part, but Octopart doesn't show it.
                    dkpn = getDkpnFromMpn( mpn )
                    
        if foundExactMpn:
            octopartResultMpn = []

        if (len(octopartPackage) == 0):
            octopartPackage = getBestPackageFromDescriptions( descriptions )
            print "Missing Octopart package info for %s, using '%s' from descriptions instead." % (mpn, octopartPackage)
        if (len(dkpn) == 0):
                print 'Missing Digi-Key part numbers'
        try:
            chunkArray[ referenceIndex ]['octopartPackage'] = octopartPackage
            chunkArray[ referenceIndex ]['digiKeyPartNumber'] = dkpn
            chunkArray[ referenceIndex ]['mouserPartNumber'] = mouserPn
            chunkArray[ referenceIndex ]['resultMpn'] = octopartResultMpn
        except:
            print 'Error in getMultipleOctopartPackages!'
            pass

    # Check that we got all the results we were expecting.
    for i in range( arraySz ):
        if not rollCall[i]:
            print "Missing an Octopart response for %s, item %s of %s." %\
                  (chunkArray[i]['octopartMpn'], i, arraySz)

    chunkDone = chunkArray.tolist()
    return ( chunkDone )

##########################################################################################################
# Get a Digi-Key part number from the MPN and URL
def  getDkpnFromMpnAndUrl( mpn, url ):
    dkpnList = []
    # Create BeautifulSoup Object
    try:
        resp = requests.get(url, timeout=20)
        ok = True
    except:
        print "Error getting url = %s." % (url)
    page = resp.text
    resp.close()
    bs = BeautifulSoup(page)

    # Find the table of parts that match
    table = bs.find(lambda tag: tag.name=='table' and tag.has_key('id') and tag['id']=="productTable")
    try:
        rows = table.findAll(lambda tag: tag.name=='tr' and tag.has_key('itemscope') and tag['itemscope']=="itemscope")
        for row in rows:
            dkmpn = row.find( 'td', attrs={'class':'mfg-partnumber'}).text.encode('latin1')
            dkpn = row.find( 'td', attrs={'class':'digikey-partnumber'}).text.encode('latin1')
            if( mpn == dkmpn):
               dkpnList.append(dkpn)
    except:
        pass
    return dkpnList

##########################################################################################################
# Get a Digi-Key part number from the MPN, but asking Digi-Key instead of Octopart.
def  getDkpnFromMpn( mpn ):
    dkpnList = []
    safeMpn = urllib.quote(mpn, '')

    # Create Url to read
    # Digikey_url = http://www.digikey.com/product-search/en?vendor=0&keywords=LMR12010YMK%2FNOPB
    # http://www.digikey.com/product-search/en?lang=en&site=us&keywords=
    dkUrlBases = [
        'http://www.digikey.com/product-search/en?lang=en&site=us&keywords=',
        #'http://www.digikey.com/product-search/en?vendor=0&keywords=',
        #'http://www.digikey.com/product-search/en?keywords='
    ]
    for urlBase in dkUrlBases:
        url = urlBase + safeMpn
        dkpnList = getDkpnFromMpnAndUrl( mpn, url )
        if len(dkpnList) > 0:
            break
    return dkpnList


##########################################################################################################
# returns an Octopart package name from an Octopart MPN, and possibly the Digi-Key part number.
def getOctopartPackageFromMpn( mpn ):
    octopartPackage = []
    dkpn = []
    API_KEY = "22becbab"
    url = 'http://octopart.com/api/v3/parts/match?'
    url += '&queries=[{"mpn":' + json.dumps(str(mpn)) + '}]'
    url += '&apikey=' + API_KEY
    url += '&include[]=specs'

    data = urllib2.urlopen(url, timeout=10).read()
    response = json.loads(data)

    # Check the Octopart response
    for result in response['results']:
        for item in result['items']:
            if( item['mpn'] == mpn ):
                # get the Octopart package name
                try:
                    newPackage = item['specs']['case_package']['value']
                    newPincount = item['specs']['pin_count']['value']
                    newPackageWithPins = combinePackageAndPins( newPackage, newPincount )
                    octopartPackage = list(set(octopartPackage + newPackageWithPins))
                except KeyError:
                    # Better luck next time.
                    # print "Skipped item missing package: %s" % (item)
                    pass

                # Get the Digi-Key part number
                try:
                    for offer in item['offers']:
                        if offer['seller']['name'] == 'Digi-Key':
                            newDkpn = offer['sku']
                            dkpn = list(set(dkpn + [newDkpn.encode('latin-1')]))
                except KeyError:
                    # Better luck next time.
                    # print "Skipped item missing offers: %s" % (item)
                    pass
    if len(dkpn) == 0:
        # Octopart is missing Digi-Key part numbers for this part.
        # Check if Digi-Key really  has the part, but Octopart is out of date.
        dkpn = getDkpnFromMpn( mpn )

    return( octopartPackage, dkpn )

# Check if a string contains any digits
# See http://stackoverflow.com/questions/11232474/is-there-a-better-way-to-find-if-string-contains-digits
_digits = re.compile('\d')
def contains_digits(d):
    return bool(_digits.search(d))

# Combine a list of packages with a list of pin counts, to make a combined package name.
def combinePackageAndPins( packageList, pinList ):
    combinedList = []
    for package in packageList:
        if not package[0].isalpha():
            combinedList.append( package.encode('latin-1') )
        else:
            for pin in pinList:
                combinedList.append( package.encode('latin-1') + '-' + pin.encode('latin-1') )
    return combinedList

# returns an octopart MPN from an ACM file
def getOctotpartMpnFromAcmFile( acmFile ):
    tree = ET.parse(acmFile)
    root = tree.getroot()
    octopart = ""
    for child in root:
        if (child.tag == "Property") and (child.attrib.get("Name") == "octopart_mpn"):
            octopart = child[0][0][0].text	#Find the octopart value
    return( octopart)


# returns a relative schematic file path from an ACM file
def getSchematicPathFromAcmFile( acmFile ):
    tree = ET.parse(acmFile)
    root = tree.getroot()
    schematicPath = ""
    idToFind = "Nothing"
    for child in root:
        if (child.tag == "DomainModel") and (child.attrib.get("Name") == "EDAModel"):
            idToFind = child.attrib.get("UsesResource")
        if (child.tag == "ResourceDependency") and (child.attrib.get("ID") == idToFind):
            schematicPath = child.attrib.get("Path")
            break
    return( schematicPath)

# returns an Eagle package name from a library file.
def getEaglePackageNameFromLibrary( lbrFile ):
    eaglePackageName = ""
    eagleLbrHash = ""
    eagleLbrString = ""
    if( os.path.isfile(lbrFile) ):
        tree = ET.parse(lbrFile)
        root = tree.getroot()
        for child in root.iter('{eagle}package'):
            ename = child.attrib.get("name")
            if( len( ename) > 0):
                eaglePackageName = ename
                eagleLbrString = ET.tostring(child) # Get the package element, as a string, for hash tag computation.
        if( len( eaglePackageName) == 0):
            for child in root.iter('package'):
                ename = child.attrib.get("name")
                if( len( ename) > 0):
                    eaglePackageName = ename
                    eagleLbrString = ET.tostring(child) # Get the package element, as a string, for hash tag computation.

        eagleLbrHash = getHashTag(eagleLbrString)
    else:
        eaglePackageName = "Missing LBR file"

    return( eaglePackageName, eagleLbrHash)

# returns an Eagle package name from an ACM file
def getEaglePackageFromAcmFile( acmFile ):
    dir_name = os.path.dirname(os.path.realpath(acmFile))
    relPath = getSchematicPathFromAcmFile( acmFile )
    lbrFilePath = os.path.join(dir_name, relPath)
#    return( getEaglePackageNameFromLibrary( lbrFilePath ) )
    eaglePackageName, eagleLbrHash = getEaglePackageNameFromLibrary( lbrFilePath )
    return (eaglePackageName, eagleLbrHash)


# gets a normalized version of a package-name string, for matching with other package-name strings.
def normalizePackageString( ps ):
    letters = ''
    digits = ''
    nps = ps.upper().strip()

    punctuation = '-_.%/$!?#()*&^@{}<>,'
    for p in punctuation:
        nps = nps.replace(p, ' ')
    words = nps.split()
    if (len(words) > 1) and (len(words[0]) > 0) and (len(words[1]) > 0):
        if words[0][0].isdigit() and words[1][0].isalpha():
            words.append( words[0])
            del words[0]
            # Keep '-EP' at the end of this list
            if( words[-2] == 'EP'):
                words[-1], words[-2] = words[-2], words[-1]
    nps = str.join('', words)
    for c in nps:
        if c.isalpha():
            letters += c
        if c.isdigit():
            digits += c
    return (letters + digits)

#####################################################################################################

# # Table of synonyms mapping Digi-Key package names to Eagle package names
# synonyms = [
#     '8-SOIC, SO08',
#     '14-SOIC, SO14',
#     '28-QFN-UT-EP, 28-QFN-EP'
# ]

normMatchesBuilt = False
normMatches = []

def normMatchesInit( synFilename ):
    global normMatchesBuilt
    global normmatches
    normMatchesBuilt = True
    with open(synFilename) as f:
        synonyms = [x.strip('\n') for x in f.readlines()]
    for line in synonyms:
        line = line.replace(',',' ')
        words = line.split()
        normWords = [normalizePackageString(x) for x in words]
        normMatches.append(normWords)

# Returns True if some non-null elelment of the first list matches and element of the second list.
def someNonNullMatch( list1, list2, synFilename ):
    global normMatchesBuilt
    global normmatches
    if not normMatchesBuilt:
        normMatchesInit( synFilename )
    for s1 in list1:
        if len(s1) > 0:
            for s2 in list2:
                if s1 == s2:
                    return True
                else:
                    for norm in normMatches:
                        if (s1 in norm) and (s2 in norm):
                            return True
    return False

#####################################################################################################
# Compares the Eagle package name to the Digi-key and Octopart package names.
def analyzeData( data, synFilename ):
    goodCount = 0
    badCount = 0
    octoNormList = []
    digiNormList = []
    eagleNormList = []

    comment = 'No check performed'


    EpFound = False

    # Get normalized versions of package names for comparison

    digiNorm = ''

    for digiPack in data['digiKeyPackage']:
        if '-EP' in digiPack:
            EpFound = True
        tmp = normalizePackageString( digiPack )
        digiNorm += tmp
        digiNormList.append(tmp)

    octoNorm = ''
    for octoPack in data['octopartPackage']:
        tmp = normalizePackageString( octoPack )
        octoNorm += tmp
        octoNormList.append(tmp)
        if EpFound:
            octoNormList.append( normalizePackageString( octoPack + '-EP' ) )

    eagleNorm = normalizePackageString( data['eaglePackage'] )
    eagleNormList.append(eagleNorm)

    state = 0   # No misses
    if not someNonNullMatch( eagleNormList, digiNormList, synFilename ):
        state |= 1
    if not someNonNullMatch( eagleNormList, octoNormList, synFilename ):
        state |= 2
    if not someNonNullMatch( digiNormList, octoNormList, synFilename ):
        state |= 4

    comments = {
        0: 'OK',
        1: "EAGLE package doesn't match Digi-Key",
        2: 'Good enough',
        3: 'EAGLE package seems wrong',
        4: '4?',
        5: 'Digi-Key package seems wrong',
        6: 'Octopart package seems wrong',
        7: 'Nothing matches'
    }

    data['comment'] = comments[ state ]

    if (len(digiNorm) == 0) and (len(octoNorm) == 0):
        data['comment'] = 'Who sells this part?'

    if( 'Missing LBR file' == data['eaglePackage']):
        data['comment'] = 'Error! Missing EAGLE LBR'

    if len(data['resultMpn']) > 0:
        data['comment'] += '; ' + str( data['resultMpn'] )

    if (0 == state) or (6 == state):
        goodCount = 1
    if (3 == state) or (7 == state):
        badCount = 1

    return (goodCount, badCount )

#####################################################################################################
# Processes the final package-name analysis queue
def processFpaRequestQueue( outFile, synFilen ):
    dataByAcmPath = {}
    sortedData =[None] * len(fpaRequestQueue.queue)
    while not fpaRequestQueue.empty():
        data = fpaRequestQueue.get()
        i = int( data['index'] ) - 1
        sortedData[ i ] = copy.deepcopy( data )

    totalCount = 0
    goodCount = 0
    badCount = 0
    packageFamilies = {}
    dataList = []
    fieldNames = ['index', 'octopartMpn', 'eagleLbrHash', 'eaglePackage',  'digiKeyPackage', 'octopartPackage', 'comment', 'acmFilepath', 'digiKeyPartNumber', 'mouserPartNumber', 'resultMpn' ]
    columnNames = {
                    'index' : 'Item No.',
                    'octopartMpn' :'Octopart MPN',
                    'eagleLbrHash' : 'EAGLE Pkg Given Name',
                    'eaglePackage' : 'EAGLE Package',
                    'digiKeyPackage' : 'Digi-Key Package',
                    'octopartPackage' : 'Octopart Package',
                    'comment' : 'Comment',
                    'acmFilepath' : 'Component ACM-file Relative File Path',
                    'digiKeyPartNumber' : 'Digi-Key Part Numbers',
                    'mouserPartNumber' : 'Mouser Part Number',
                    'resultMpn' : 'Actual part Number'}

    print "Begin matching package names."

    with open(outFile, 'wb') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=fieldNames, quoting=csv.QUOTE_ALL)

        writer.writerow(columnNames)

        for data in sortedData:
            totalCount += 1
            good, bad = analyzeData( data, synFilen )
            goodCount += good
            badCount += bad
            dataList.append( data )
            print "%s" % (data)
            lastName = data['eaglePackage']
            firstName = data['eagleLbrHash']
            if (len(lastName) > 0):
                if lastName in packageFamilies:
                    familyMembers = packageFamilies[ lastName ]
                    if firstName in familyMembers:
                        familyMembers[ firstName ] += 1
                    else:
                        familyMembers[ firstName ] = 1
                    packageFamilies[ lastName ] = familyMembers
                else:
                    packageFamilies[ lastName ] = {firstName: 1}

            fpaRequestQueue.task_done()
            writer.writerow(data)
            dataByAcmPath[os.path.abspath(data['acmFilepath'])] = data

    print "Total: %s, good: %s, bad: %s" % (totalCount, goodCount, badCount)

    print
    print "Package counts by given names:"
    pprint.pprint(packageFamilies)
    
    return dataByAcmPath


# Creates and queues an octopart lookup request.  This request will include:
# - the ACM file path
# - the Octopart MPN
# - the Eagle package name
createPackageCounter = 0;

def createOctopartRequest( acmFile ):
    global createPackageCounter
    createPackageCounter += 1
    request = {}
    request['acmFilepath'] = acmFile
    request['octopartMpn'] = getOctotpartMpnFromAcmFile( acmFile )
    eaglePackage, eagleLbrHash = getEaglePackageFromAcmFile( acmFile )
    request['eaglePackage'] = eaglePackage
    request['eagleLbrHash'] = eagleLbrHash
    request['index'] = str(createPackageCounter)
    octopartRequestQueue.put( request )

# Parse the command line arguments
def parse_args(args):    # parses the command line arguments
    """Returns a list of program arguments"""
    parser = OptionParser()
    parser.add_option("-s", "--src", dest="src",
                  help="path searched for ACM files", metavar="SRC")
    parser.add_option("-d", "--dst", dest="dst",
                  help="path to a CSV file that will get the test results", metavar="DST")
    parser.add_option("-m", "--map", dest="map",
                  help="path to a text file containing comma-or-space-delimited groups of package name synonyms, one group per line.", metavar="MAP")

    (options, args) = parser.parse_args(args)
    if options.src is None:
        parser.error("missing required src argument")
        return None

    print
    print "Options:"
    print ('\tsrc: {0}'.format(options.src))
    print ('\tdst: {0}'.format(options.dst))
    print ('\tmap: {0}'.format(options.map))
    print
    return options


###################################################################
#
# The main program
#
###################################################################

def main(args=sys.argv[1:]):
    print "*** Begin checking component packages. ***"
    global threadExitFlag
    global threadID
    global threads
    options = parse_args(args)
    # print( options )
    try:
        fp = open(options.dst, 'wb')
        fp.close()
    except:
        print '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
        print "ERROR! The output file '%s' is not writable." % (options.dst)
        print '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
        sys.exit(-2)
    acmFiles = []
    for root, dirs, files in os.walk(options.src):
        acmFiles += [os.path.join(root, file_) for file_ in files if file_.endswith(".acm")]
    acmFiles.sort()

    print "Found %s ACM files to check." % (len(acmFiles))
    print
    
    processAcmFiles(acmFiles, options)
    
def processAcmFiles(acmFiles, options={}):
    global threadExitFlag
    global threadID
    global threads
    global q2dkEvent

    # Start a single Octopart lookup thread
    startOctopartLookupThread( threadID )
    threadID += 1

    # Start a bunch of Digi-Key lookup threads
    numberOfDigiKeyThreads = 100
    for i in range(numberOfDigiKeyThreads):
        startDkLookupThread( threadID )
        threadID += 1

    print

    for acmFile in acmFiles:
        #print( "============== About to process file " + acmFile + " =======================================================================================================\n")
        createOctopartRequest( acmFile )

    # Wait for small queues to stabilize
    q2dkEvent.wait( 5 )

    # Wait for octopartRequestQueue to empty
    #print( "Wait for octopartRequestQueue to empty.")
    while not octopartRequestQueue.empty():
        #print( "Not empty yet.")
        time.sleep(1)
    octopartRequestQueue.join()

    # Wait for dkRequestQueue to empty
    #print( "Wait for dkRequestQueue to empty.")
    while not dkRequestQueue.empty():
        #print( "Not empty yet.")
        time.sleep(1)
    dkRequestQueue.join()

    # Wait for small queues to stabilize
    time.sleep( 3 )

    # Notify threads it's time to exit
    # print( "Setting exit flag.")
    threadExitFlag = 1

    # Wait for all threads to complete
    for t in threads:
        t.join()

    print
    print "Internet part lookups are complete."
    print

    dataByAcmPath = processFpaRequestQueue( options.dst, options.map )

    print( "Done.")
    
    return dataByAcmPath


#start the main program when run as a module
if __name__ == "__main__":
    main()
