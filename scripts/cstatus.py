import os
import re
import csv
import xml.etree.ElementTree as ET

#list of status reports
sdict = {'Missing Icon': [], 
		'Missing Datasheet': [], 
		'Missing Markdown' : [], 
		'Missing Schematic': [],
		'Missing ConnectorDefinition': [],
		'Missing Classification' : [],
		'Missing ECAD Connection(s)': [],
		'Name/Octopart Mismatch' : []}

#list of components with their respective directory
cdict = {}

def CreateDict(path):
	#Remove empty component folders
	for dir in os.listdir(path):
		name = os.path.abspath(os.path.join(path, dir))
		try:
			os.rmdir(name)
			#print "deleting " + name
		except WindowsError:
			#print "skipping " + name
			continue
	#Search for components (.acm files)
	for root, dirs, files in os.walk(path):
		for __file__ in files:
			if __file__.endswith(".acm"):
				cdict[__file__] = root#.strip("components")
				with open(os.path.abspath(os.path.join(root, __file__))) as acm:
					#print "\n" + root
					checkACM(acm, __file__)
				fpath = os.path.abspath(root)
				compdir = os.listdir(os.path.abspath(root))
				if "icon.png" not in compdir:
					sdict['Missing Icon'].append(__file__)
				if "doc" in compdir:
					docs = os.listdir(os.path.abspath(os.path.join(root, "doc")))
					if not any(doc.endswith(".pdf") for doc in docs):
							sdict['Missing Datasheet'].append(__file__)
					if not any(doc.endswith(".md") for doc in docs):
							sdict['Missing Markdown'].append(__file__)
				elif "Doc" in compdir:
					docs = os.listdir(os.path.abspath(os.path.join(root, "Doc")))
					if not any(doc.endswith(".pdf") for doc in docs):
							sdict['Missing Datasheet'].append(__file__)
					if not any(doc.endswith(".md") for doc in docs):
							sdict['Missing Markdown'].append(__file__)
				else:
					sdict['Missing Datasheet'].append(__file__)
					sdict['Missing Markdown'].append(__file__)
				if "Schematic" in compdir:
					schdir = os.listdir(os.path.abspath(os.path.join(root, "Schematic")))
					if any(".lbr" not in sch for sch in schdir):
						sdict['Missing Schematic'].append(__file__)
				else:
					sdict['Missing Schematic'].append(__file__)

def checkACM(acm, __file__):
	tree = ET.parse(acm)
	root = tree.getroot()
	octopart = ""
	if __file__ not in sdict.get('Missing Schematic'):
		#print "this " + __file__ + " indeed has an eagle schematic"
		if checkEDAwires(root, __file__) == False:
			sdict['Missing ECAD Connection(s)'].append(__file__)
	for child in root:
		if (child.tag == "Classifications"):
			if not child.text:
				sdict['Missing Classification'].append(__file__)
		if (child.tag == "Property") and (child.attrib.get("Name") == "octopart_mpn"):
			octopart = child[0][0][0].text.replace("/", "_")	#Find the octopart value

	if octopart != __file__.strip(".acm"):
		#print octopart, __file__.strip(".acm")
		sdict['Name/Octopart Mismatch'].append(__file__)



def checkEDAwires(root, __file__):
	p = 0
	EDA = {'IDs' : [],
		'PortMaps' :[]}
	Connections = ""
	c_def = True

	#print __file__
	#Look through domain models
	for model in root.iter("DomainModel"):
		attribs = str(model.attrib.items())
		#Only interested in EDA model
		if "EDAModel" in attribs:
			pins = model.iter("Pin")
			for pin in pins:
				p+=1
				EDA['IDs'].append(pin.attrib.get("ID"))
				EDA['PortMaps'].append(pin.attrib.get("PortMap"))
	for connector in root.iter("Connector"):
		if not connector.attrib.get("Definition"):
			c_def = False
		roles = connector.iter("Role")
		for role in roles:
			#Add IDs to "Connections" (String)
			Connections+=role.attrib.get("ID")
			Connections+=role.attrib.get("PortMap")

	if not c_def: 
		sdict['Missing ConnectorDefinition'].append(__file__)

	#Check to see that EDA pin ID from either section is in "Connections"
	for p in range(p):
		IDs = filter(None, re.split(r'[\s]\s*',EDA['IDs'][p]))
		PortMaps = filter(None, re.split(r'[\s]\s*',EDA['PortMaps'][p]))
		if any(ID in Connections for ID in IDs):
			#print "found connection"
			continue
		elif any(pmap in Connections for pmap in PortMaps):
			#print "found connection"
			continue
		else:
			return False
	return True



def GenerateCSV():
	with open('component_statuses.csv', 'w') as csvfile:
		writer = csv.writer(csvfile)
		for key, value in sdict.items():
			writer.writerow([key, value])
		#for key, value in cdict.items():
		#	writer.writerow([key, value])


CreateDict("components")
GenerateCSV()
