import unittest
import fnmatch
import os
import os.path
from lxml import etree as ET

path = "."
null = None

def RemoveDupes(path):
	remove = False
	acm = ET.parse(path)
	root = acm.getroot()
	empty = []
	classifications = acm.xpath("Classifications")
	if len(classifications) > 1:
		print path
		for cs in classifications:
			if cs.text == null:
				remove = True
				empty.append(cs)

	# only alter acm files that have empty classifications
	if remove:
		for e in empty:
			root.remove(e)

		acm.write(path)


for dir in os.listdir(path):
	name = os.path.abspath(os.path.join(path, dir))
	try:
		os.rmdir(name)
		#print "deleting " + name
	except WindowsError:
		#print "skipping " + name
		continue
#Search for components (.acm files)
for root, dirs, files in os.walk(path):
	for f in files:
		if f.endswith(".acm"):
			path = os.path.abspath(os.path.join(root, f))
			RemoveDupes(path)