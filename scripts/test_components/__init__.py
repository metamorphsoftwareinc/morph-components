__author__ = 'adam'

import unittest
import fnmatch
import os
import os.path
import re
from lxml import etree
import numpy as np

repo_dir = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

exceptions_file = os.path.join( os.path.dirname(os.path.abspath(__file__)), "test.exceptions")   # CT-82

exceptions_map = {}

# Parses the exceptions_file to create the exceptions_map. CT-82
def init_exceptions_map( exceptions_file ):
    if os.path.exists( exceptions_file ):
        #read the file
        lines = []
        found_test = ""
        with open(exceptions_file) as f:
            lines = f.read().splitlines()
        for line in lines:
            # Check for the name of a test
            match = re.search(r'\[(.+)\]', line)
            if( match ):
                found_test = match.group(1)
            else:
                #check for a directory name relative to the repo
                trimLine = line.strip()
                if len( trimLine ) > 0 :
                    testPath = os.path.normpath( os.path.join( repo_dir, trimLine ) )
                    if( os.path.isabs(testPath)):
                        # The test path looks like a path
                        if exceptions_map.has_key( testPath ):
                            exceptions_map[ testPath ].append( found_test ) # CT-117
                        else:
                            exceptions_map[ testPath ] = [found_test]
                    else:
                        print( "Error in exceptions_file: unrecognized path '{0}' for test '{1}'.".format( testPath, found_test ))
                        found_test = '?'
    # end of init_exceptions_map



# remove the found exceptions from a list of tests to be run. CT-82
def remove_exceptions_from_list( full_test_list, exception_list ):
    result = full_test_list[:]
    result = [x for x in result if not x.func_name in exception_list]
    if 'all' in [x.lower() for x in exception_list]:
            result = []
    return result

# Get a list of excepted tests for a component.  CT-82
def get_exception_list_from_path( path ):
    found_exceptions = []
    test_path = os.path.normpath(path)
    # Check each subdirectory for exception entries
    while( len(test_path) > len(repo_dir)):
        if exceptions_map.has_key( test_path ):
            found_exceptions.extend( exceptions_map[ test_path ])
        test_path = os.path.dirname( test_path )
    #be neat
    found_exceptions = list( set( sorted( found_exceptions )))
    return found_exceptions

# Gets a list of the ACM files we might test.
# But, if a subdirctory looks like it contains many components,
# the components were probably generated automatically, and
# we only use a sample of the components in that directory.
def find_acm_files(path):
    matches = []
    print path
    # This class keeps track of the ACM-file search mode.
    # Either we take all the acm files in the directory,
    # or, if there are many in the grandparent directory,
    # we take a sample.
    class AcmSearchMode:
        ALL = 1
        SAMPLE = 2
        bigDirectory = ""
        BIG_SIZE = 499  # BIG_SIZE is the maximum number of acm files we want to allow from a grandparent directory.
                        # We assume that the parent directory of an acm file typically contains a single acm file.
                        # BIG_SIZE was set to 499 because the Pycharm debugger won't show bigger lists.
        samplePopulation = []   # This is where we temporarily save all the acm files before deciding which ones
                                # to include in our sample.

    searchMode = AcmSearchMode.ALL  # Keep track of the current ACM-file search state, either ALL or SAMPLE.

    for root, dirnames, filenames in os.walk(path):
        if AcmSearchMode.ALL == searchMode:
            if len(dirnames) < AcmSearchMode.BIG_SIZE:
                acm_filenames = fnmatch.filter(filenames, '*.acm')
                for filename in acm_filenames:
                    matches.append(os.path.join(root, filename))
            else:
                # print( "Found a big directory with ", len(dirnames), " subdirectories: ", root, ".\n" )
                AcmSearchMode.bigDirectory = root
                AcmSearchMode.samplePopulation = []
                searchMode = AcmSearchMode.SAMPLE
        else:
            acm_filenames = fnmatch.filter(filenames, '*.acm')
            if AcmSearchMode.bigDirectory in root:
                # We are still sampling acm files
                for filename in acm_filenames:
                    AcmSearchMode.samplePopulation.append(os.path.join(root, filename))
            else:
                # We have a sample population to process.
                np.random.seed(123)    # Use a repeatable pseudo-random sequence for sampling
                permutedIndexArray = np.random.permutation(len(AcmSearchMode.samplePopulation))
                # Take just the first BIG_SIZE randomly-permuted indices, and sort them.
                indices = sorted(permutedIndexArray.tolist()[:AcmSearchMode.BIG_SIZE], key=int)
                # Now we've determined the indices of the sample population to be included in our sample.
                for index in indices:
                    matches.append( AcmSearchMode.samplePopulation[ index ] )
                # Clean up
                searchMode = AcmSearchMode.ALL
                AcmSearchMode.samplePopulation = []
                # Stay calm and carry on with the directory that didn't match.
                for filename in acm_filenames:
                    matches.append(os.path.join(root, filename))
    return matches


class TestAllComponents(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        pass
        #import sys
        #sys.path.append(os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), 'packages'))
        #import packages
        #cls.package_results = packages.processAcmFiles(acm_files, type('Options', (), {'dst': './packages.csv'}))

    pass


id_map = {}
#---------------------------------------------------------------------
def test_function_generator(path):

    def has_unique_id(self):
        acm = etree.parse(path)
        id = acm.xpath("/avm:Component/@ID", namespaces={"avm": "avm"})[0]
        self.assertFalse(id in id_map, "Duplicate Component ID %s" % id)
        id_map[id] = path

    def PackageMatches(self):
        self.assertEquals('OK', self.package_results[os.path.abspath(path)]['comment'])

    # For each EDA Pin in the component, make sure it's connected to something
    def eda_pins_connected(self):
        acm = etree.parse(path)
        eda_pin_verdict = {}
        eda_pin_name_map = {}

        # Get all EDA Pins. Add them to the map.
        # If any have PortMap content, count them as OK
        for e in acm.xpath("DomainModel"):
            for key in e.keys():
                if key.endswith("}type") and e.get(key).endswith(":EDAModel"):
                    for pin in e.xpath("Pin"):
                        id_pin = pin.get("ID")
                        name_pin = pin.get("Name")
                        eda_pin_name_map[id_pin] = name_pin

                        portmap = pin.get("PortMap")
                        if portmap is None or portmap is "":
                            eda_pin_verdict[id_pin] = False
                        else:
                            eda_pin_verdict[id_pin] = True

        # Get all Connector Roles.
        # See if any have a PortMap that points to the EDA Pins we're tracking.
        for c in acm.xpath("Connector/Role/@PortMap"):
            for idMapped in c.split():
                if idMapped in eda_pin_verdict:
                    eda_pin_verdict[idMapped] = True

        # Every EDA Pin must be involved in a PortMap.
        # Check them all to make sure that we found a map.
        # Some types are exempt. Check for those cases first.
        for key, val in eda_pin_verdict.iteritems():
            name = eda_pin_name_map[key]

            is_nc = name in ["NC", "NC1"] or name.startswith("NC")
            if is_nc:
                # it's okay -- it means "No Connection"
                continue

            self.assertTrue(val, "EDA Pin %s (%s) is unconnected" % (name, key))

    def connector_eda_pins_connected(self):
        acm = etree.parse(path)
        eda_pin_verdict = {}
        eda_pin_name_map = {}

        # Get all EDA Pins defined within Connectors as Roles. Add them to the map.
        # If any have PortMap content, count them as OK

        # First, get all of the roles in all of the connectors
        for connector in acm.xpath("Connector"):
            for role in connector.xpath("Role"):
                # See if this role is an EDA pin
                for key in role.keys():
                    if key.endswith("}type") and role.get(key).endswith(":Pin"):
                        # Index it
                        id_pin = role.get("ID")
                        name_pin = role.get("Name")
                        name_connector = connector.get("Name")
                        eda_pin_name_map[id_pin] = "%s::%s" % (name_connector, name_pin)

                        portmap = role.get("PortMap")
                        if portmap is None or portmap is "":
                            eda_pin_verdict[id_pin] = False
                        else:
                            eda_pin_verdict[id_pin] = True


        # Get all EDAPins inside EDAModels
        # See if any have a PortMap that points to the Pins we're tracking.
        for e in acm.xpath("DomainModel"):
            for key in e.keys():
                if key.endswith("}type") and e.get(key).endswith(":EDAModel"):
                    for pm in e.xpath("Pin/@PortMap"):
                        for idMapped in pm.split():
                            if idMapped in eda_pin_verdict:
                                eda_pin_verdict[idMapped] = True

        # Check all of the Connector pins too
        for c in acm.xpath("Connector/Role/@PortMap"):
            for idMapped in c.split():
                if idMapped in eda_pin_verdict:
                    eda_pin_verdict[idMapped] = True

        # Every EDA Pin must be involved in a PortMap.
        # Check them all to make sure that we found a map.
        for key, val in eda_pin_verdict.iteritems():
            name = eda_pin_name_map[key]
            self.assertTrue(val, "EDA Pin %s (%s) is unconnected" % (name, key))

    def has_icon(self):
        acm = etree.parse(path)
        icon_resource = acm.xpath("ResourceDependency[@Name = 'Icon.png']")
        self.assertEquals(1, len(icon_resource), "No Icon found (looking for Resource with name 'Icon.png')")

    def has_eda_model(self):
        found_eda = False

        acm = etree.parse(path)
        for e in acm.xpath("DomainModel"):
            for key in e.keys():
                if key.endswith("}type") and e.get(key).endswith(":EDAModel"):
                    found_eda = True

        self.assertTrue(found_eda, "No EDAModel in ACM")

    def eda_model_deviceset_matches(self):
        acm_rootfolder = os.path.dirname(path)

        acm = etree.parse(path)

        for dm in acm.xpath("DomainModel"):
            for key in dm.keys():
                if key.endswith("}type") and dm.get(key).endswith(":EDAModel"):
                    # This is an EDAModel.
                    # Let's fetch its resource.
                    res_id = dm.get("UsesResource")

                    # Fetch this resource
                    xpath_expr = "//ResourceDependency[@ID = '%s']/@Path" % res_id
                    matches = acm.xpath(xpath_expr)
                    res_relpath = matches[0].replace('\\', '/')
                    res_fullpath = os.path.join(acm_rootfolder, res_relpath)

                    # Find DeviceSets
                    acm_ds = dm.get("DeviceSet")
                    found_match = False

                    eagle = etree.parse(res_fullpath)
                    eagle_ds_set_names = eagle.xpath(".//eagle:deviceset/@name", namespaces={"eagle": "eagle"})
                    eagle_ds_set_names_nons = eagle.xpath(".//deviceset/@name")

                    for eagle_ds_name in list(set(eagle_ds_set_names) | set(eagle_ds_set_names_nons)):
                        if eagle_ds_name == acm_ds:
                            found_match = True

                    self.assertTrue(found_match, "EAGLE model doesn't have a DeviceSet called %s, but the EDAModel object references one." % acm_ds)

    def spice_model_exists(self):
        found_spice = False
        acm = etree.parse(path)
        for e in acm.xpath("DomainModel"):
            for key in e.keys():
                if key.endswith("}type") and e.get(key).endswith(":SPICEModel"):
                    found_spice = True

        self.assertTrue(found_spice, "No SPICEModel in ACM")

    def spice_model_parameters_connected(self):
        acm = etree.parse(path)
        for e in acm.xpath("DomainModel"):
            found_spice = False
            for key in e.keys():
                if key.endswith("}type") and e.get(key).endswith(":SPICEModel"):
                    found_spice = True
            if found_spice:
                for param in e:
                    if param.tag == "Parameter":
                        parameter_name = param.get("Locator")
                        value_ok = False
                        value = param.find("Value")
                        value_expr = value.find("ValueExpression")
                        for vkey in value_expr.keys():
                            keyval = value_expr.get(vkey)
                            if keyval.endswith(":DerivedValue"):
                                value_ok = True
                                break
                        self.assertTrue(value_ok, "No SPICE parameter value connected for %s." % parameter_name)

    def resource_files_exist(self):
        acm = etree.parse(path)
        resources = acm.xpath("ResourceDependency")

        missing = []
        for r in resources:
            r_relpath = r.get("Path")
            r_abspath = os.path.join(os.path.dirname(path), r_relpath.replace('\\', '/'))
            if not os.path.isfile(r_abspath):
                missing.append(r_relpath)

        self.assertEquals(0, len(missing), "Resources file(s) not found: \n%s" % '\n'.join(missing))

    def no_eda_pin_to_pin_connections(self):
        acm = etree.parse(path)

        pins = []
        for e in acm.xpath("DomainModel"):
            for key in e.keys():
                if key.endswith("}type") and e.get(key).endswith(":EDAModel"):
                    for pin in e.xpath("Pin"):
                        pins.append(pin)

        pin_ids = []
        eda_pin_name_map = {}
        for pin in pins:
            id_pin = pin.get("ID")
            pin_ids.append(id_pin)
            name_pin = pin.get("Name")
            eda_pin_name_map[id_pin] = name_pin

        violating_maps = []
        for pin in pins:
            portmaps = pin.get("PortMap")
            if portmaps is None or portmaps == "":
                continue
            else:
                for portmap in portmaps.split():
                    if portmap in pin_ids:
                        violator_name = eda_pin_name_map[portmap]
                        msg = '%s (%s)' % (violator_name, portmap)
                        violating_maps.append(msg)

        self.assertEqual(0, len(violating_maps), "EDA Pins are connected to other EDA Pins: \n%s" % ('\n'.join(violating_maps)))

    def has_classification(self):
        acm = etree.parse(path)


        classifications = acm.xpath('Classifications')
        num_classifications = len(classifications)
        self.assertNotEqual(0, num_classifications, "No Classification")
        self.assertEqual(1, num_classifications, "More than 1 Classification (%d)" % (num_classifications))

    def has_datasheet(self):
        acm = etree.parse(path)

        resources = acm.xpath("ResourceDependency")
        found_datasheet = False
        for resource in resources:
            if resource.get("Path").lower().endswith('.pdf'):
                found_datasheet = True

        self.assertTrue(found_datasheet, "No Datasheet found (looking for Resource with extension of '.pdf')")

    def has_markdown(self):
        acm = etree.parse(path)

        resources = acm.xpath("ResourceDependency")
        found_markdown = False
        for resource in resources:
            if resource.get("Path").lower().endswith('.md'):
                found_markdown = True

        self.assertTrue(found_markdown, "No Markdown doc found (looking for Resource with extension of '.md')")

    def has_mpn(self):
        acm = etree.parse(path)
        #ResourceDependency[@Name = 'Icon.png']")

        mpns = []
        properties = acm.xpath("Property")
        for prop in properties:
            if prop.get('Name').lower() == "octopart_mpn":
                mpns.append(prop.get('Name'))

        self.assertNotEqual(0, len(mpns), "Missing Property called 'octopart_mpn'")
        self.assertEqual(1, len(mpns), "Too many Properties called 'octopart_mpn'")

    # Remove the excepted tests from the list of tests to run on this component.  CT-82
    exception_list = get_exception_list_from_path( path )   #MOT-CT-82
    full_test_list = [eda_pins_connected,
            has_icon,
            resource_files_exist,
            eda_model_deviceset_matches,
            has_eda_model,
            no_eda_pin_to_pin_connections,
            spice_model_exists,
            spice_model_parameters_connected,
            has_classification,
            has_datasheet,
            has_markdown,
            has_mpn,
            has_unique_id,
            connector_eda_pins_connected,
            #PackageMatches
            ]
    modified_test_list = remove_exceptions_from_list( full_test_list, exception_list )

    return modified_test_list

init_exceptions_map( exceptions_file )    # Initialize the exceptions map. CT-82

acm_files = find_acm_files(repo_dir)
# acm_files = [f for f in acm_files if not 'passive_components/capacitors/single_components' in f]
# acm_files = [f for f in acm_files if not 'passive_components/resistors/single_components' in f]
print "Found %i ACM files to test" % len(acm_files)
# acm_files = [f for f in acm_files if 'AD590JH' in f]
# acm_files = [f for f in acm_files if 'LM3668SD' in f]
# acm_files = [f for f in acm_files if 'MCP9700' in f]

for f in acm_files:
    # print t
    test_name_prefix = 'test_%s' % f[len(repo_dir)+1:]
    tests = test_function_generator(f)
    for test in tests:
        setattr(TestAllComponents, test_name_prefix.replace('\\', '/').replace('/', '.').replace('&','&amp;') + '_' + test.__name__, test)
    
if __name__ == "__main__":

    unittest.main()
    suite = unittest.TestLoader().loadTestsFromTestCase(TestAllComponents)
    unittest.TextTestRunner().run(suite)
